<?php
// require_once Yii::app()->basePath.'/extensions/libSSE/libsse.php';
class ChatController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','send','receive','conference'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
		$this->render('index');
	}	
	public function actionConference()
	{
		$this->render('conference');
	}	
	// public function actionSend()
	// {
 //    $sse = new SSE();
 //    $sse->addEventListener('',new LatestMessage());
 //    $sse->start();
	// }
	// public function actionReceive()
	// {
 //    if(isset($_POST['message'])) {
 //        $ret = new Chat;
 //        $ret->message = $_POST['message'];
 //        $ret->user_id = Yii::app()->user->id;
 //        $ret->timestamp = time();
 //        $ret->save();
 //    }
	// }
}

// class LatestMessage extends SSEEvent {
// 	private $cache = 0;
//   private $data;
//   public function update(){
//       if ($this->data != null)
//           return json_encode(array(
//               'name'=>$this->data->user->username,
//               'message'=>$this->data->message,
//           ));
//   }
//   public function check(){
//       $ret = Chat::model()->find(array('order'=>'id desc', 'limit'=>1));
//       $this->data = $ret;
//       if($this->data->timestamp !== $this->cache){
//           $this->cache = $this->data->timestamp;
//           return true;
//       }
//       return false;
//   }
// }