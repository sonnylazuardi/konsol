<?php

class ProgressController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control <rules></rules>
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','sonny'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('harian','mingguan','bulanan','batang','tabel','kurva'),
				'users'=>array('@'),
			),
			array('allow',
				'actions' => array('create','update','admin','delete','manage','add'),
				'roles' => array('engineering'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$id = Yii::app()->session['project'];
		$model=new Progress;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Progress']))
		{
			$model->attributes=$_POST['Progress'];
			$model->project_id = $id;
			if($model->save())
				$this->redirect(array('manage', 'id'=>$id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Progress']))
		{
			$model->attributes=$_POST['Progress'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if(isset($_GET['back']))
			// 	$this->redirect(array($_GET['back']));
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Progress');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Progress('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Progress']))
			$model->attributes=$_GET['Progress'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Progress::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='progress-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionManage()
	{
		$id = Yii::app()->session['project'];
		$model = Progress::model()->findAll('project_id=:id', array(':id'=>$id));
		if (isset($_POST['placed'])) {
			Progress::model()->deleteAllPlaced($id);
			foreach ($_POST['placed'] as $a => $item1) {
				foreach ($item1 as $b => $item2) {
					foreach ($item2 as $c => $item3) {
						$newPlan = new RencanaProgress;
						$newPlan->progress_id = $a;
						$newPlan->bulan = $b;
						$newPlan->minggu = $c;
						$newPlan->tahun = 2013;
						$newPlan->save();
					}
				}
			}
		}
		$arrPlaced = Progress::model()->arrPlaced($id);
		$this->render('manage',array(
			'model'=>$model,
			'id'=>$id,
			'arrPlaced'=>$arrPlaced,
		));
	}
	public function actionHarian($print = 0, $tanggal = 0)
	{
		require_once(Yii::getPathOfAlias('ext.dompdf').'/pdf.php');
		$model = new Progress('harian');
		$id = Yii::app()->session['project'];
		if ($print == 0) {
			$this->render('harian', array('model'=>$model, 'print'=>$print, 'tanggal'=>$tanggal));
		} else {
			$pdf = new Pdf();
			$pdf->renderPartial('harian', array('model'=>$model, 'print'=>$print, 'tanggal'=>$tanggal));
			$pdf->stream('Laporan_Harian_'.$tanggal);
		}
	}
	public function actionBulanan($print = 0, $bulan = 0)
	{
		$this->redirect(array('tabel', 'bulan'=>$bulan));
	}
	public function actionMingguan($print = 0, $minggu = 0)
	{
		require_once(Yii::getPathOfAlias('ext.dompdf').'/pdf.php');
		$id = Yii::app()->session['project'];
		$arrAktual = array(0,0,0,0,0,0);
		$arrRencana = array(0,0,0,0,0,0);
		if ($minggu != 0) {
			$m = explode('-', $minggu);
			for ($i=1; $i <= 5; $i++) { 
				$arrAktual[$i] = AktualProgress::model()->totalUntilWeek($i, $m[1]);
				$arrRencana[$i] = RencanaProgress::model()->totalUntilWeek($i, $m[1]);
			}
		}
		if ($print == 0) {
			$this->render('mingguan', array('print'=>$print, 'minggu'=>$minggu, 'arrAktual'=>$arrAktual, 'arrRencana'=>$arrRencana));
		} else {
			$pdf = new Pdf();
			$pdf->renderPartial('mingguan', array('print'=>$print, 'minggu'=>$minggu, 'arrAktual'=>$arrAktual, 'arrRencana'=>$arrRencana));
			$pdf->stream('Laporan_Mingguan_'.$minggu);
		}
	}
	public function actionAdd()
	{
		$model=new AktualProgress;
		
		if(isset($_POST['AktualProgress']))
		{
			$model->attributes=$_POST['AktualProgress'];
			$model->tanggal = date('Y-m-d');
			$model->timestamp = date('Y-m-d h:i:s');
			if($model->save())
				$this->redirect(array('harian', 'tanggal'=>date('Y-m-d')));
		}
		
		$this->render('_aktual',array(
			'model'=>$model,
		));
	}
	public function actionTabel($bulan = 0, $tahun = 2013)
	{
		$this->layout = '//layouts/column1';
		// Perhitungan Aktual Progress
		for ($i=1; $i <= 5; $i++) { 
			$arrAktual[$i] = AktualProgress::model()->totalUntilWeek($i, $bulan, $tahun);
			$arrRencana[$i] = RencanaProgress::model()->totalUntilWeek($i, $bulan, $tahun);
		}
		$this->render('tabel', array('bulan'=>$bulan, 'arrAktual'=>$arrAktual, 'arrRencana'=>$arrRencana));
	}
	public function actionBatang($bulan = 0, $tahun = 2013)
	{
		$this->layout = '//layouts/column1';
		// Perhitungan Aktual Progress
		for ($i=1; $i <= 4; $i++) { 
			$arrAktual[] = AktualProgress::model()->totalUntilWeek($i, $bulan, $tahun);
			$arrRencana[] = RencanaProgress::model()->totalUntilWeek($i, $bulan, $tahun);
		}
		$this->render('batang', array('bulan'=>$bulan, 'arrAktual'=>$arrAktual, 'arrRencana'=>$arrRencana));
	}
	public function actionKurva($tahun = 2013)
	{
		$this->layout = '//layouts/column1'; 
		// Perhitungan Aktual Progress
		$lastMonth = AktualProgress::model()->lastMonth();
		for ($i=7; $i <= 12; $i++) { 
			if ($i <= $lastMonth)
				$arrAktual[] = AktualProgress::model()->totalUntilWeek(4, $i, $tahun);
			$arrRencana[] = RencanaProgress::model()->totalUntilWeek(4, $i, $tahun);
		}
		$this->render('kurva', array('arrAktual'=>$arrAktual, 'arrRencana'=>$arrRencana));
	}
}

