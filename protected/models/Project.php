<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $type
 * @property string $budget
 * @property integer $owner
 * @property string $pic
 *
 * The followings are the available model relations:
 * @property Site[] $sites
 */
class Project extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, structure, gambaran', 'required'),
			array('owner', 'numerical', 'integerOnly'=>true),
			array('title, type, budget, pic', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, description, type, budget, owner, pic, structure', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sites' => array(self::HAS_MANY, 'Site', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'gambaran' => 'Gambaran',
			'structure' => 'Structure',
			'type' => 'Type',
			'budget' => 'Budget',
			'owner' => 'Owner',
			'pic' => 'Pic',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('owner',$this->owner);
		$criteria->compare('pic',$this->pic,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->create_time=date("Y-m-d H:i:s");
				$this->owner=Yii::app()->user->id;
			}
			return true;
		}
		else
			return false;
	}

	public function getAllProject()
	{
		$arr = array();
		$model = Project::model()->findAll();
		foreach ($model as $var) {
			$arr[] = array('label'=>$var->title, 'url'=>array('/project/view', 'id'=>$var->id));
		}
		if (Yii::app()->user->checkAccess('admin')) 
			$arr[] = array('label'=>'Tambah Project', 'url'=>array('/project/create'));	
		return $arr;
	}
}