<?php

/**
 * This is the model class for table "external".
 *
 * The followings are the available columns in table 'external':
 * @property integer $id
 * @property integer $procurement_id
 * @property string $nama_vendor
 * @property string $alamat_vendor
 * @property string $nama_pic
 * @property string $tanggal_berdiri
 * @property string $pengalaman
 * @property integer $telepon
 * @property string $email
 * @property integer $harga_penarwaran
 * @property string $gambar
 *
 * The followings are the available model relations:
 * @property Procurement $procurement
 */
class External extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return External the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'external';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procurement_id, nama_vendor, alamat_vendor, nama_pic, tanggal_berdiri, pengalaman, telepon, email, harga_penawaran', 'required'),
			array('procurement_id, telepon, harga_penawaran', 'numerical', 'integerOnly'=>true),
			array('nama_vendor, alamat_vendor, nama_pic, email, gambar', 'length', 'max'=>128),
			array('email','email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, procurement_id, nama_vendor, alamat_vendor, nama_pic, tanggal_berdiri, pengalaman, telepon, email, harga_penawaran, gambar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procurement' => array(self::BELONGS_TO, 'Procurement', 'procurement_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'procurement_id' => 'Procurement',
			'nama_vendor' => 'Nama Vendor',
			'alamat_vendor' => 'Alamat Vendor',
			'nama_pic' => 'Nama Pic',
			'tanggal_berdiri' => 'Tanggal Berdiri',
			'pengalaman' => 'Pengalaman',
			'telepon' => 'Telepon',
			'email' => 'Email',
			'harga_penarwaran' => 'Harga Penarwaran',
			'gambar' => 'Gambar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('procurement_id',$this->procurement_id);
		$criteria->compare('nama_vendor',$this->nama_vendor,true);
		$criteria->compare('alamat_vendor',$this->alamat_vendor,true);
		$criteria->compare('nama_pic',$this->nama_pic,true);
		$criteria->compare('tanggal_berdiri',$this->tanggal_berdiri,true);
		$criteria->compare('pengalaman',$this->pengalaman,true);
		$criteria->compare('telepon',$this->telepon);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('harga_penarwaran',$this->harga_penarwaran);
		$criteria->compare('gambar',$this->gambar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->timestamp=date("Y-m-d H:i:s");
			}
			return true;
		}
		else
			return false;
	}
}