<?php

/**
 * This is the model class for table "progress".
 *
 * The followings are the available columns in table 'progress':
 * @property integer $id
 * @property integer $project_id
 * @property integer $item_pekerjaan_id
 * @property string $pekerjaan_dilaksanakan
 * @property string $tanggal
 * @property string $timestamp
 * @property integer $volume
 *
 * The followings are the available model relations:
 * @property ItemPekerjaan $itemPekerjaan
 * @property Project $project
 */
class Progress extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Progress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'progress';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, item_pekerjaan_id, pekerjaan_dilaksanakan, tanggal, timestamp, volume', 'required'),
			array('project_id, item_pekerjaan_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, item_pekerjaan_id, pekerjaan_dilaksanakan, tanggal, timestamp, volume', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPekerjaan' => array(self::BELONGS_TO, 'ItemPekerjaan', 'item_pekerjaan_id'),
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'item_pekerjaan_id' => 'Item Pekerjaan',
			'pekerjaan_dilaksanakan' => 'Pekerjaan Dilaksanakan',
			'tanggal' => 'Tanggal',
			'timestamp' => 'Timestamp',
			'volume' => 'Volume',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$id);
		$criteria->compare('item_pekerjaan_id',$this->item_pekerjaan_id);
		$criteria->compare('pekerjaan_dilaksanakan',$this->pekerjaan_dilaksanakan,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('volume',$this->volume);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function harian($id, $tanggal)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		// $criteria->compare('id',$this->id);
		$criteria->compare('project_id',$id);
		// $criteria->compare('item_pekerjaan_id',$this->item_pekerjaan_id);
		// $criteria->compare('pekerjaan_dilaksanakan',$this->pekerjaan_dilaksanakan,true);
		$criteria->compare('tanggal',$tanggal,true);
		// $criteria->compare('timestamp',$this->timestamp,true);
		// $criteria->compare('volume',$this->volume);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
			'sort'=>false,
		));
	}
	public function arrTanggal() 
	{
		$ret = Progress::model()->findAll(array('group'=>'tanggal'));
		$arr = array();
		foreach ($ret as $tgl) {
			$arr[$tgl->tanggal] = $tgl->tanggal;
		}
		return $arr;
	}
	/**
     * Returns the amount of weeks into the month a date is
     * @param $date a YYYY-MM-DD formatted date
     * @param $rollover The day on which the week rolls over
     */
    function getWeeks($date, $rollover)
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $i = 1;
        $weeks = 1;

        for($i; $i<=$elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover))  $weeks ++;
        }

        return $weeks;
    }
	public function getJumlahTK()
	{
		// $c = new CDbCriteria;
		// $c->with = 'manpower';
		// $c->compare('manpower.project_id', $this->project_id);
		$d = date_parse_from_format("Y-m-d", $this->tanggal);
		// $c->compare('bulan', $d['month']);
		// $c->compare('tahun', $d['year']);
		// $c->group = 'manpower.project_id';
		// $c->select = 'SUM(manpower.jumlah) AS sumTK';
		// $e = RencanaManpower::model()->find($c);
		// if ($e)
		// 	return $e->sumTK;
		// else
		// 	return 0;
		$list= Yii::app()->db
			->createCommand('select SUM(manpower.jumlah) AS sumTK from rencana_manpower left join manpower on manpower.id = rencana_manpower.manpower_id where project_id=:id and minggu=:minggu and bulan=:bulan and tahun=:tahun group by project_id')
			->bindValue('id',$this->project_id)
			->bindValue('bulan',$d['month'])
			->bindValue('tahun',$d['year'])
			->bindValue('minggu',$this->getWeeks($this->tanggal, 'sunday'))
			->queryAll();
		if ($list)
			return $list[0]['sumTK'];
		else
			return 0;
	}
}