<?php

/**
 * This is the model class for table "progress".
 *
 * The followings are the available columns in table 'progress':
 * @property integer $id
 * @property integer $project_id
 * @property string $jenis_pekerjaan
 * @property string $deskripsi_progress
 * @property string $satuan
 * @property string $keterangan
 * @property double $volume
 * @property double $harga_satuan
 *
 * The followings are the available model relations:
 * @property AktualProgress[] $aktualProgresses
 * @property Project $project
 * @property RencanaProgress[] $rencanaProgresses
 */
class Progress extends CActiveRecord
{
	public static $sumTotal = 0;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Progress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'progress';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('project_id, deskripsi_progress, volume, harga_satuan', 'required'),
				array('project_id', 'numerical', 'integerOnly'=>true),
				array('volume, harga_satuan', 'numerical'),
				array('jenis_pekerjaan', 'length', 'max'=>200),
				array('deskripsi_progress', 'length', 'max'=>255),
				array('satuan', 'length', 'max'=>10),
				array('keterangan', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('id, project_id, jenis_pekerjaan, deskripsi_progress, satuan, keterangan, volume, harga_satuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'aktualProgresses' => array(self::HAS_MANY, 'AktualProgress', 'progress_id'),
				'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
				'rencanaProgresses' => array(self::HAS_MANY, 'RencanaProgress', 'progress_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'project_id' => 'Project',
				'jenis_pekerjaan' => 'Jenis Pekerjaan',
				'deskripsi_progress' => 'Deskripsi Pekerjaan',
				'satuan' => 'Satuan',
				'keterangan' => 'Keterangan',
				'volume' => 'Volume',
				'harga_satuan' => 'Harga Satuan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('jenis_pekerjaan',$this->jenis_pekerjaan,true);
		$criteria->compare('deskripsi_progress',$this->deskripsi_progress,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('harga_satuan',$this->harga_satuan);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}
	public function arrPlaced($id = 0)
	{
		$model = Progress::model()->findAll('project_id=:id', array(':id'=>$id));
		$arr = array();
		foreach ($model as $item) {
			$arrs = $item->rencanaProgresses;
			foreach ($arrs as $plan) {
				if ($plan != null) {
					$arr[] = $plan->progress_id . '-' . $plan->bulan . '-' . $plan->minggu;
				}
			}
		}
		return $arr;
	}

	public function deleteAllPlaced($id = 0)
	{
		$model = Progress::model()->findAll('project_id=:id', array(':id'=>$id));
		$allid = array();
		foreach ($model as $item) {
			$allid[] = $item->id;
		}
		// print_r($allid);
		// exit();
		$c = new CDbCriteria();
		$c->addInCondition("progress_id", $allid);
		RencanaProgress::model()->deleteAll($c);
	}
	public function harian($tanggal)
	{
		$id = Yii::app()->session['project'];
		$criteria=new CDbCriteria;
		$criteria->with = array('progress');
		$criteria->together = true;
		$criteria->compare('progress.project_id',$id);
		$criteria->compare('tanggal',$tanggal,true);

		return new CActiveDataProvider('AktualProgress', array(
				'criteria'=>$criteria,
				'pagination'=>false,
				'sort'=>false,
		));
	}
	public function mingguan($tanggal)
	{
		$id = Yii::app()->session['project'];
		$criteria=new CDbCriteria;
		$criteria->with = array('progress');
		$criteria->together = true;
		$criteria->compare('progress.project_id',$id);
		$criteria->compare('tanggal',$tanggal,true);

		return new CActiveDataProvider('AktualProgress', array(
				'criteria'=>$criteria,
				'pagination'=>false,
				'sort'=>false,
		));
	}
	public function arrTanggal()
	{
		$ret = AktualProgress::model()->findAll(array('group'=>'tanggal'));
		$arr = array();
		$arr[0] = 'Semua Tanggal';
		foreach ($ret as $tgl) {
			$arr[$tgl->tanggal] = date('l, j F Y', strtotime($tgl->tanggal));
		}
		return $arr;
	}
	public function arrBulan()
	{
		$ret = RencanaProgress::model()->findAll(array('group'=>'bulan, tahun'));
		$arr = array();
		$arr[0] = 'Pilih Bulan';
		foreach ($ret as $tgl) {
			$arr[$tgl->bulan] = 'Bulan ke-'.$tgl->bulan.' Tahun '.$tgl->tahun;
		}
		return $arr;
	}
	public function arrMinggu()
	{
		$ret = AktualProgress::model()->findAll(array('group'=>'tanggal'));
		$arr = array();
		$arr[0] = 'Pilih Minggu';
		foreach ($ret as $tgl) {
			$week = Helper::getWeeks($tgl->tanggal, 'sunday');
			$d = date_parse_from_format("Y-m-d", $tgl->tanggal);

			if (!array_key_exists($week.'-'.$d['month'], $arr))
				$arr[$week.'-'.$d['month']] = 'Minggu ke-'.$week.date(' F Y', strtotime($tgl->tanggal));
		}
		return $arr;
	}
	public function getTotalHarga()
	{
		return $this->harga_satuan * $this->volume;
	}
	public function scopes()
	{
		return array(
			'projectnow'=>array(
				'condition'=>'project_id=:p',
				'params'=>array(':p'=>Yii::app()->session['project']),
			),
		);
	}
	public function keuangan($bulan = 0) {
		$id = Yii::app()->session['project'];
		$list= Yii::app()->db
		->createCommand('select SUM(progress.volume * progress.harga_satuan) AS sumTotal from rencana_progress left join progress on progress.id = rencana_progress.progress_id where project_id=:id and bulan=:bulan group by project_id')
		->bindValue('id',$id)
		->bindValue('bulan',$bulan)
		->queryRow();
		if ($list)
			return $list['sumTotal'];
		else
			return 0;
	}
}