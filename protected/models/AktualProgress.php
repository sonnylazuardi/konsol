<?php

/**
 * This is the model class for table "aktual_progress".
 *
 * The followings are the available columns in table 'aktual_progress':
 * @property integer $id
 * @property integer $progress_id
 * @property string $pekerjaan_dilaksanakan
 * @property string $tanggal
 * @property string $timestamp
 * @property double $volume
 *
 * The followings are the available model relations:
 * @property Progress $progress
 */
class AktualProgress extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AktualProgress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'aktual_progress';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('progress_id, pekerjaan_dilaksanakan, tanggal, timestamp, volume', 'required'),
			array('progress_id', 'numerical', 'integerOnly'=>true),
			array('volume', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, progress_id, pekerjaan_dilaksanakan, tanggal, timestamp, volume', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'progress' => array(self::BELONGS_TO, 'Progress', 'progress_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'progress_id' => 'Progress',
			'pekerjaan_dilaksanakan' => 'Pekerjaan Dilaksanakan',
			'tanggal' => 'Tanggal',
			'timestamp' => 'Timestamp',
			'volume' => 'Volume',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('progress_id',$this->progress_id);
		$criteria->compare('pekerjaan_dilaksanakan',$this->pekerjaan_dilaksanakan,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('volume',$this->volume);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getJumlahTK()
	{
		$id = Yii::app()->session['project'];
		$d = date_parse_from_format("Y-m-d", $this->tanggal);
		$list= Yii::app()->db
		->createCommand('select SUM(manpower.jumlah) AS sumTK from rencana_manpower left join manpower on manpower.id = rencana_manpower.manpower_id where project_id=:id and minggu=:minggu and bulan=:bulan and tahun=:tahun group by project_id')
		->bindValue('id',$id)
		->bindValue('bulan',$d['month'])
		->bindValue('tahun',$d['year'])
		->bindValue('minggu',Helper::getWeeks($this->tanggal, 'sunday'))
		->queryRow();
		if ($list)
			return $list['sumTK'];
		else
			return 0;
	}
	public function scopes()
	{
		return array(
			'projectnow'=>array(
				'with'=>array('progress'),
				'condition'=>'progress.project_id=:p',
				'params'=>array(':p'=>Yii::app()->session['project']),
			),
		);
	}
	public function getTotalHarga()
	{
		return $this->progress->harga_satuan * $this->volume;
	}
	public function totalUntilWeek($week = 0, $bulan = 0, $tahun = 2013)
	{
		$c = new CDbCriteria;
		$c->condition = 'MONTH(tanggal) <= :b AND YEAR(tanggal) = :t';
		$c->params = array(':b'=>$bulan, ':t'=>$tahun);
		// $c->group = 'tanggal';
		$c->order = 'tanggal asc';

		$model = AktualProgress::model()->projectnow()->findAll($c);
		$sumTotal = RencanaProgress::model()->sumTotal();
		$sum = 0;
		foreach ($model as $item) {
			if (Helper::getWeeks($item->tanggal, 'sunday') > $week && intval(date('m',strtotime($item->tanggal))) >= $bulan) break;
			$sum += ($item->totalHarga / $sumTotal * 100);	
		}
		return $sum;
	}
	public function lastMonth()
	{
		$c = new CDbCriteria();
		$c->order = 'tanggal desc';
		$model = AktualProgress::model()->find($c);
		$bulan = intval(date('m',strtotime($model->tanggal)));
		return $bulan;
	}
}