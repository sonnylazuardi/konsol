<?php

/**
 * This is the model class for table "item_pekerjaan".
 *
 * The followings are the available columns in table 'item_pekerjaan':
 * @property integer $id
 * @property string $jenis_pekerjaan
 * @property string $deskripsi_pekerjaan
 * @property string $satuan
 * @property string $keterangan
 * @property double $volume
 * @property double $harga_satuan
 *
 * The followings are the available model relations:
 * @property Progress[] $progresses
 * @property Rencana[] $rencanas
 */
class ItemPekerjaan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPekerjaan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_pekerjaan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_pekerjaan, volume, harga_satuan', 'required'),
			array('volume, harga_satuan', 'numerical'),
			array('jenis_pekerjaan', 'length', 'max'=>200),
			array('deskripsi_pekerjaan', 'length', 'max'=>255),
			array('satuan', 'length', 'max'=>10),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jenis_pekerjaan, deskripsi_pekerjaan, satuan, keterangan, volume, harga_satuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'progresses' => array(self::HAS_MANY, 'Progress', 'item_pekerjaan_id'),
			'rencanas' => array(self::HAS_MANY, 'Rencana', 'item_pekerjaan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis_pekerjaan' => 'Jenis Pekerjaan',
			'deskripsi_pekerjaan' => 'Deskripsi Pekerjaan',
			'satuan' => 'Satuan',
			'keterangan' => 'Keterangan',
			'volume' => 'Volume',
			'harga_satuan' => 'Harga Satuan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis_pekerjaan',$this->jenis_pekerjaan,true);
		$criteria->compare('deskripsi_pekerjaan',$this->deskripsi_pekerjaan,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('volume',$this->volume);
		$criteria->compare('harga_satuan',$this->harga_satuan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}