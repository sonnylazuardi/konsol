<?php

/**
 * This is the model class for table "manpower".
 *
 * The followings are the available columns in table 'manpower':
 * @property integer $id
 * @property integer $project_id
 * @property string $deskripsi_manpower
 * @property string $satuan
 * @property string $keterangan
 * @property integer $durasi
 * @property double $jumlah
 * @property double $harga_satuan
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property RencanaManpower[] $rencanaManpowers
 */
class Manpower extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Manpower the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manpower';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, durasi, jumlah, harga_satuan', 'required'),
			array('project_id, durasi', 'numerical', 'integerOnly'=>true),
			array('jumlah, harga_satuan', 'numerical'),
			array('deskripsi_manpower', 'length', 'max'=>255),
			array('satuan', 'length', 'max'=>10),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_id, deskripsi_manpower, satuan, keterangan, durasi, jumlah, harga_satuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'rencanaManpowers' => array(self::HAS_MANY, 'RencanaManpower', 'manpower_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'deskripsi_manpower' => 'Deskripsi Manpower',
			'satuan' => 'Satuan',
			'keterangan' => 'Keterangan',
			'durasi' => 'Durasi',
			'jumlah' => 'Jumlah',
			'harga_satuan' => 'Harga Satuan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('deskripsi_manpower',$this->deskripsi_manpower,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('durasi',$this->durasi);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('harga_satuan',$this->harga_satuan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function arrPlaced($id = 0)
	{
		$model = Manpower::model()->findAll('project_id=:id', array(':id'=>$id));
		$arr = array();
		foreach ($model as $item) {
			$arrs = $item->rencanaManpowers;
			foreach ($arrs as $plan) {
				if ($plan != null) {
					$arr[] = $plan->manpower_id . '-' . $plan->bulan . '-' . $plan->minggu;
				}
			}
		}
		return $arr;
	}
	public function keuangan($bulan = 0) {
		$id = Yii::app()->session['project'];
		$list= Yii::app()->db
		->createCommand('select SUM(manpower.jumlah * manpower.harga_satuan) AS sumTotal from rencana_manpower left join manpower on manpower.id = rencana_manpower.manpower_id where project_id=:id and bulan=:bulan group by project_id')
		->bindValue('id',$id)
		->bindValue('bulan',$bulan)
		->queryRow();
		if ($list)
			return $list['sumTotal'];
		else
			return 0;
	}
	public function deleteAllPlaced($id = 0)
	{
		$model = Manpower::model()->findAll('project_id=:id', array(':id'=>$id));
		$allid = array();
		foreach ($model as $item) {
			$allid[] = $item->id;
		}
		$c = new CDbCriteria();
		$c->addInCondition("manpower_id", $allid);
		RencanaManpower::model()->deleteAll($c);
	}
}