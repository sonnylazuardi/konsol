<?php

/**
 * This is the model class for table "rencana_progress".
 *
 * The followings are the available columns in table 'rencana_progress':
 * @property integer $id
 * @property integer $progress_id
 * @property integer $minggu
 * @property integer $bulan
 * @property integer $tahun
 *
 * The followings are the available model relations:
 * @property Progress $progress
 */
class RencanaProgress extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RencanaProgress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rencana_progress';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('progress_id, minggu, bulan, tahun', 'required'),
			array('progress_id, minggu, bulan, tahun', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, progress_id, minggu, bulan, tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'progress' => array(self::BELONGS_TO, 'Progress', 'progress_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'progress_id' => 'Progress',
			'minggu' => 'Minggu',
			'bulan' => 'Bulan',
			'tahun' => 'Tahun',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('progress_id',$this->progress_id);
		$criteria->compare('minggu',$this->minggu);
		$criteria->compare('bulan',$this->bulan);
		$criteria->compare('tahun',$this->tahun);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function scopes()
	{
		return array(
			'projectnow'=>array(
				'with'=>array('progress'),
				'together'=>true,
				'condition'=>'progress.project_id=:p',
				'params'=>array(':p'=>Yii::app()->session['project']),
			),
		);
	}
	public function sumTotal()
	{
		$model = RencanaProgress::model()
			->projectnow()
			->findAll();
		$sum = 0;
		foreach ($model as $item) {
			$sum += $item->progress->totalHarga;
		}
		return $sum;
	}
	public function sumFreq($progress_id)
	{
		$c = new CDbCriteria();
		$c->condition = 'progress_id = :p';
		$c->params = array(':p'=>$progress_id);
		return count(RencanaProgress::model()->findAll($c));
	}
	public function totalUntilWeek($week = 0, $bulan = 0, $tahun = 2013)
	{
		$c = new CDbCriteria;
		$c->condition = 'bulan <= :b AND tahun = :t';
		$c->params = array(':b'=>$bulan, ':t'=>$tahun);
		$c->order = 'bulan asc, minggu asc';
		// $c->group = 'progress_id';
		$model = RencanaProgress::model()->projectnow()->findAll($c);
		$sum = 0;
		$sumTotal = $this->sumTotal();
		// echo $sumTotal; exit();
		foreach ($model as $item) {
			if ($item->minggu > $week && $item->bulan >= $bulan) break;	
			$freq = $this->sumFreq($item->progress_id);
			$sum += ($item->progress->totalHarga / $sumTotal * 100) / $freq;
		}
		return $sum;
	}
}