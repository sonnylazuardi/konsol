<?php

// DOMPDF_autoload
Yii::registerAutoloader(function($class) {
  $filename = DOMPDF_INC_DIR . "/" . mb_strtolower($class) . ".cls.php";
  
  if ( is_file($filename) )
    require_once($filename);
});

require Yii::getPathOfAlias('ext.dompdf').DIRECTORY_SEPARATOR.'dompdf_config.inc.php';

class Pdf
{
  private $_dompdf;
  
  private $_html;
  
  public $output;
  
  /**
   * Init
   */
  public function __construct()
  {
    $this->_dompdf = new DOMPDF();
    $this->_dompdf->base_path = Yii::app()->request->baseUrl; 
  }
  
  /**
   * set paper size
   * 
   * @param string $size
   * @param string $orientation
   */
  public function setSize($size, $orientation='portrait')
  {
    $this->_dompdf->set_paper($size, $orientation);
  }
  
  public function renderPartial($view, $params)
  {
    Yii::app()->controller->layout = "//layouts/report";
    $html = Yii::app()->controller->renderPartial($view, $params, true, false);
    $this->_html .= $html;
  }
  
  public function stream($name)
  {
    $this->_dompdf->load_html($this->_html);
    $this->_dompdf->render();
    $this->_dompdf->stream($name);
  }
  //Returning pdf output as a stream, e,g when using you want to send pdf as attachment in emails
  public function output()
  {
    $this->_dompdf->load_html($this->_html);
    $this->_dompdf->render();
    $this->output = $this->_dompdf->output();
    return $this->output;
  }
}