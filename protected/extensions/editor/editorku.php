<?php
class Editorku extends CWidget
{
	public $filespath;
	public $filesurl;
	public $boleh;
	public function run()
	{
		$cs = Yii::app()->getClientScript();
		//$cs->registerScriptFile("http://latex.codecogs.com/integration/ckeditor/ckeditor.js");
		$cs->registerScriptFile(Yii::app()->baseurl."/../editor/ckeditor/ckeditor.js");
		$kcFinderPath=Yii::app()->baseurl."/../editor/kcfinder/";
		$js='CKEDITOR.config.filebrowserBrowseUrl="'.$kcFinderPath.'browse.php?type=files";';
		$js.='CKEDITOR.config.filebrowserImageBrowseUrl="'.$kcFinderPath.'browse.php?type=images";';
		$js.='CKEDITOR.config.filebrowserFlashBrowseUrl="'.$kcFinderPath.'browse.php?type=flash";';
		$js.='CKEDITOR.config.filebrowserUploadUrl="'.$kcFinderPath.'upload.php?type=files";';
		$js.='CKEDITOR.config.filebrowserImageUploadUrl="'.$kcFinderPath.'upload.php?type=images";';
		$js.='CKEDITOR.config.filebrowserFlashUploadUrl="'.$kcFinderPath.'upload.php?type=flash";';
		Yii::app()->clientScript->registerScript('editorku', $js, CClientScript::POS_END);
	    $session=new CHttpSession;
	    $session->open();
        $session['KCFINDER'] = array(
		'disabled'=>!$this->boleh,
		'uploadURL'=>$this->filesurl,
		'uploadDir'=>$this->filespath,
		);
	}
}