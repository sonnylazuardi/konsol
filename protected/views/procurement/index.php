<?php
$this->menu=array(
	array('label'=>'Create Procurement','url'=>array('create')),
);
?>

<h1>Procurements</h1>

<div class="row">
<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

</div>