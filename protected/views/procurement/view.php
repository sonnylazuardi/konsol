<h1><?php echo $model->judul; ?></h1>

<?php echo $model->deskripsi ?>

<b>Harga Penawaran : </b>
Rp <?php echo number_format($model->harga,2) ?>

<?php if (Yii::app()->user->checkAccess('admin')): ?>
	<h2>Penawaran</h2>
	<?php echo CHtml::link('Tutup', array('update', 'id'=>$model->id, '#'=>'tutup'), array('class'=>'btn btn-danger')) ?>
	<div class="row">
	<?php 
		$penawaran = External::model()->findAll('procurement_id = :p order by id desc', array(':p'=>$model->id));
		foreach ($penawaran as $item) {
			$this->renderPartial('_external', array('data'=>$item));
		}
	?>
	</div>

<?php endif ?>


<h2>Ambil procurement ini : </h2> 
<?php $this->renderPartial('external', array('model2'=>$model2)) 
?>
