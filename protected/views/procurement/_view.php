<div class="span9">
<div class="well">
	<h4><?php echo CHtml::link(CHtml::encode($data->judul),array('view','id'=>$data->id)); ?></h4>

	<p><?php echo $data->deskripsi ?></p>

	<b>Harga : </b> Rp <?php echo number_format($data->harga,2) ?>
	<br />

	<b>Deadline : </b> <?php echo $data->deadline ?>
	
	
	<?php 
			if(Yii::app()->user->checkAccess('admin')) {
				echo '<hr/>';
				echo CHtml::link('edit', array('update', 'id'=>$data->id), array('class'=>'btn'));
				echo CHtml::link('hapus', array('delete', 'id'=>$data->id), array('class'=>'btn'));
			}
		 ?>
</div>
</div>