<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array( 
    'id'=>'external-form', 
    'enableAjaxValidation'=>false, 
)); ?>

    <?php echo $form->errorSummary($model2); ?>

    <?php echo $form->textFieldRow($model2,'nama_vendor',array('class'=>'span6','maxlength'=>128)); ?>

    <?php echo $form->textFieldRow($model2,'alamat_vendor',array('class'=>'span6','maxlength'=>128)); ?>

    <?php echo $form->textFieldRow($model2,'nama_pic',array('class'=>'span6','maxlength'=>128)); ?>

    <?php echo $form->textFieldRow($model2,'tanggal_berdiri',array('class'=>'span6', 'placeholder'=>1999-12-20)); ?>

    <?php echo $form->textAreaRow($model2,'pengalaman',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

    <?php echo $form->textFieldRow($model2,'telepon',array('class'=>'span6', 'placeholder'=>'08157123123')); ?>

    <?php echo $form->textFieldRow($model2,'email',array('class'=>'span6','maxlength'=>128)); ?>

    <?php echo $form->textFieldRow($model2,'harga_penawaran',array('class'=>'span6')); ?>

    <?php echo $form->textFieldRow($model2,'gambar',array('id'=>'pics','class'=>'span6','maxlength'=>128)); ?>
        <a href="#" class="btn btn-primary" onclick="openKCFinder_singleFile()">Browse Server</a>


    <div class="form-actions"> 
        <?php $this->widget('bootstrap.widgets.TbButton', array( 
            'buttonType'=>'submit', 
            'type'=>'primary', 
            'label'=>'Ambil',
        )); ?>
    </div> 

<?php $this->endWidget(); ?>

<?php $this->renderPartial('//layouts/ckeditor') ?>
<?php $this->renderPartial('//layouts/kcfinder') ?>