<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'procurement-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'judul',array('class'=>'span6','maxlength'=>128)); ?>

	<?php echo $form->textAreaRow($model,'deskripsi',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->textFieldRow($model,'harga',array('class'=>'span6')); ?>

	<?php echo $form->textFieldRow($model,'deadline',array('class'=>'span6')); ?>

	<?php echo $form->textFieldRow($model,'gambar',array('class'=>'span6','maxlength'=>128, 'id'=>'pics')); ?>
	<a href="#" class="btn btn-primary" onclick="openKCFinder_singleFile()">Browse Server</a>
	<div id="tutup">
	<?php echo $form->dropDownListRow($model,'status',array(0=>'Ditutup',1=>'Dibuka'),array('class'=>'span6')); ?>
	</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('//layouts/ckeditor') ?>
<?php $this->renderPartial('//layouts/kcfinder') ?>