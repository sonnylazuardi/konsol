<?php 
$this->menu=array(
	array('label'=>'Tambah Progress','url'=>array('add')),
);
?>
<?php $this->renderPartial('//project/toolbar'); ?>

<?php $this->renderPartial('laporan'); ?>

<?php if(!$print): ?>
	<div class="row">
		<div class="span3">
		<form id="tgl" method="get">
			<?php echo CHtml::dropDownList("tanggal", $tanggal, Progress::model()->arrTanggal(), array('onchange'=>'$("#tgl").submit()')); ?>
		</form>
		</div>
		<div class="span3">
		<form id="print" method="get">
			<input type="hidden" name="tanggal" value="<?php echo $tanggal ?>">
			<button type="submit" class="btn btn-primary" name="print" value="1">Print</button>
		</form>
		</div>
	</div>
<?php else: ?>
	<p>Tanggal <?php echo date('l, j F Y', strtotime($tanggal)) ?></p>
	<img src="themes/bootstrap/img/logo.png" alt="">
<?php endif; ?>

<?php
function SumVolume($provider)
{
    $total=0;
    foreach($provider->data as $item)
        $total+=$item->volume;
    return $total;
}
?>

<?php
$provider = $model->harian($tanggal);
$this->widget('ext.groupgridview.GroupGridView',array(
	'id'=>'progress-grid',
	'type'=>'striped bordered',
	'template'=>'{items}',
	'mergeColumns' => array('tanggal','jumTK'),
	'dataProvider'=>$provider,
	'columns'=>array(
		array(
			'name'=>'tanggal',
			'value'=>'date("l, j F Y", strtotime($data->tanggal))',
		),
		array(
			'header' => 'Jenis Pekerjaan',
			'value' => '$data->progress->deskripsi_progress',
			'footer' => 'Total',
		),
		array(
			'name' => 'pekerjaan_dilaksanakan',
			'type' => 'raw'
		),
		array(
			'name'=>'volume',
			'footer'=>sumVolume($provider),	
		),
		array(
			'name' => 'jumTK',
			'header'=>'Jumlah TK',
			'value'=>'$data->jumlahTK',
		),
	),
)); ?>
