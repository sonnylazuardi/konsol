<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'project_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenis_pekerjaan',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'deskripsi_progress',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'satuan',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'volume',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'harga_satuan',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
