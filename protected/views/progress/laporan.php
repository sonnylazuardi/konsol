<?php
$id = Yii::app()->session['project'];
$this->widget('bootstrap.widgets.TbMenu', array(
		'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
		'stacked'=>false, // whether this is a stacked menu
		'items'=>array(
				array('label'=>'Harian', 'url'=>array('progress/harian')),
				array('label'=>'Mingguan', 'url'=>array('/progress/mingguan')),
				array('label'=>'Bulanan', 'url'=>array('/progress/bulanan')),
		),
)); ?>