<?php
$this->breadcrumbs=array(
	'Progresses',
);

$this->menu=array(
	array('label'=>'Create Progress','url'=>array('create')),
	array('label'=>'Manage Progress','url'=>array('admin')),
);
?>

<h1>Progresses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
