<?php
$id = Yii::app()->session['project'];
$this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Rencana', 'url'=>array('progress/manage')),
        array('label'=>'Aktual', 'url'=>array('/progress/aktual')),
    ),
)); ?>