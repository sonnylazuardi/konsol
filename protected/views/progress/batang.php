<?php $this->renderPartial('//project/toolbar'); ?>
<h1>Grafik Batang</h1>

<form id="tgl" method="get">
   <?php echo CHtml::dropDownList("bulan", $bulan, Progress::model()->arrBulan(), array('onchange'=>'$("#tgl").submit()')); ?>
</form>

<?php
$cat = array('Minggu 1', 'Minggu 2', 'Minggu 3', 'Minggu 4');
?>

<?php
$this->Widget('ext.highcharts.HighchartsWidget', array(
   'options'=>array(
      'title' => array('text' => 'Grafik Bulan '.$bulan),
      'xAxis' => array(
         'categories' => $cat,
      ),
      'yAxis' => array(
         'title' => array('text' => 'Bobot'),
      ),
      'series' => array(
         array( 'name'=>'Aktual',
         		'type'=>'column',
         		'data' => $arrAktual,
         ),
         array( 'name'=>'Rencana',
               'type'=>'column',
               'data' => $arrRencana,
         ),
        /*
         array( 'name'=>'Suara',
         		'type'=>'pie',
         		'size'=>100,
         		'center'=>array(100,50),
         		'data' => $pie,
         ),
         */
      )
   )
));
?>