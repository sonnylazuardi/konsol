<?php $this->renderPartial('//project/toolbar'); ?>
<h1>Kurva Kumulatif</h1>

<?php
$cat = array('Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
?>

<?php
$this->Widget('ext.highcharts.HighchartsWidget', array(
   'options'=>array(
      'title' => array('text' => 'Kurva Kumulatif'),
      'xAxis' => array(
         'categories' => $cat,
      ),
      'yAxis' => array(
         'title' => array('text' => 'Bobot'),
      ),
      'series' => array(
         array( 'name'=>'Aktual',
         		'type'=>'line',
         		'data' => $arrAktual,
         ),
         array( 'name'=>'Rencana',
               'type'=>'line',
               'data' => $arrRencana,
         ),
        /*
         array( 'name'=>'Suara',
         		'type'=>'pie',
         		'size'=>100,
         		'center'=>array(100,50),
         		'data' => $pie,
         ),
         */
      )
   )
));
?>