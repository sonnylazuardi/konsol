<?php $this->renderPartial('//project/toolbar'); ?>

<h1>Tabel Kumulatif</h1>

<?php 
function keterangan($selisih)
{
	if ($selisih >= 2) {
		echo '<td bgcolor="green">Excelent</td>';
	} else if (0 <= $selisih && $selisih < 2) {
		echo '<td bgcolor="yellow">Good</td>';
	} else if (-2 <= $selisih && $selisih < 0) {
		echo '<td bgcolor="grey">Enough</td>';
	} else if ($selisih < -2 ) {
		echo '<td bgcolor="red">Alert</td>';
	}
}
?>

<form id="tgl" method="get">
	<?php echo CHtml::dropDownList("bulan", $bulan, Progress::model()->arrBulan(), array('onchange'=>'$("#tgl").submit()')); ?>
</form>

<table class="table table-bordered">
	<tr>
		<td>Minggu</td>
		<td>Rencana</td>
		<td>Aktual</td>
		<td>Selisih</td>
		<td>Keterangan</td>
	</tr>
	<?php for($i = 1; $i <= 5; $i++): ?>
	<tr>
		<td><?php echo $i ?></td>
		<td><?php echo number_format($arrRencana[$i],3) ?></td>
		<td><?php echo number_format($arrAktual[$i],3) ?></td>
		<td><?php echo number_format(($selisih[$i] = $arrAktual[$i]-$arrRencana[$i]),3); ?></td>
		<?php echo keterangan($selisih[$i]) ?>
	</tr>
	<?php endfor; ?>
	<tr>
		<td>Total</td>
		<td><?php echo number_format(array_sum($arrRencana), 3) ?></td>
		<td><?php echo number_format(array_sum($arrAktual), 3) ?></td>
		<td><?php echo number_format(($avg = array_sum($selisih)/count($selisih)), 3) ?></td>
		<?php echo keterangan($avg) ?>
	</tr>
</table>