<?php
$this->breadcrumbs=array(
	'Progresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage Progress','url'=>array('manage')),
);
?>

<h1>Update Progress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<?php echo CHtml::link('Hapus', array('delete', 'id'=>$model->id), array('class'=>'btn btn-danger', 'onclick'=>'if(!confirm("Yakin mau dihapus?"))return false')) ?>