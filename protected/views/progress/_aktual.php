<h1>Tambah Progress</h1>
<p>Hari ini tanggal <?php echo date('l, j F Y') ?></p>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'aktual-progress-form',
		'enableAjaxValidation'=>false,
)); ?> 
    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListRow($model,'progress_id',CHtml::listData(Progress::model()->findAll(), 'id', 'deskripsi_progress'),array('class'=>'span5')); ?>

    <?php echo $form->textAreaRow($model,'pekerjaan_dilaksanakan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->textFieldRow($model,'volume',array('class'=>'span5')); ?>

    <div class="form-actions"> 
        <?php $this->widget('bootstrap.widgets.TbButton', array( 
            'buttonType'=>'submit', 
            'type'=>'primary', 
            'label'=>'Tambah', 
        )); ?>
    </div> 

<?php $this->endWidget(); ?>