<?php
$id = Yii::app()->session['project'];

$this->menu=array(
	array('label'=>'Create Progress','url'=>array('create')),
);
?>
<?php $this->renderPartial('//project/toolbar'); ?>
<h1>Rencana Progress</h1>

<form method="post">
 <table class="table table-striped table-bordered">
 	<tr>
 		<td rowspan="3">No</td>
		<td rowspan="3">Item Pekerjaan</td>
		<td rowspan="3">Satuan</td>
		<td colspan="4">Uraian</td>
		<td colspan="19">Tahun 2013</td>
 	</tr>
	<tr>
		<td rowspan="2">Volume</td>
		<td rowspan="2">Harga Satuan</td>
		<td rowspan="2">Jumlah Harga</td>
		<td rowspan="2">Bobot</td>
		<td>Juli</td>
		<td colspan="4">Agustus</td>
		<td colspan="4">September</td>
		<td colspan="4">Oktober</td>
		<td colspan="4">November</td>
		<td colspan="2">Desember</td>
	</tr>
	<tr>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
	</tr>
		<?php 
			$totalVolume = 0;
			$totalHargaSatuan = 0;
			$totalJumlahHarga = 0;
			$totalBobot = 0; 
			//hitung jumlah awal
			foreach ($model as $item) {
				$totalVolume += $item->volume;
				$totalHargaSatuan += $item->harga_satuan;
				$totalJumlahHarga += $item->harga_satuan * $item->volume;	
			}
			$c = 1;
		?>
		<?php foreach ($model as $item): ?>
		<tr>
			<td><?php echo $c++ ?></td>
			<td><?php echo CHtml::link($item->deskripsi_progress, array('update', 'id'=>$item->id))  ?></td>
			<td><?php echo $item->satuan ?></td>
			<td><?php echo number_format($item->volume, 2) ?></td>
			<td><?php echo number_format($item->harga_satuan, 2) ?></td>
			<td><?php echo number_format(($jHarga = $item->harga_satuan * $item->volume), 2) ?></td>
			<td><?php echo number_format(($jBobot = ($jHarga/$totalJumlahHarga * 100)), 3).'%' ?></td>
			<?php $totalBobot += $jBobot ?>
			<?php $j = 4; $js = 0;
				for ($i=7; $i <= 12; $i++) {
					while($j <= 4)
					{
						if (in_array($item->id.'-'.$i.'-'.$j,$arrPlaced)) $js++; $j++;
						if ($i == 12 && $j == 3) break;
					}				
					$j = 1;
				}
				$j = 4; 
				for ($i=7; $i <= 12; $i++) {
					while($j <= 4)
					{
						echo '<td>'.CHtml::checkBox('placed['.$item->id.']['.$i.']['.$j.']', ($s = in_array($item->id.'-'.$i.'-'.$j,$arrPlaced)));
						if ($s) echo '<br>'.number_format($jBobot/$js,3).'%';
						echo '</td>';
						$j++;
						if ($i == 12 && $j == 3) break;
					}				
					$j = 1;
				}
			?>
		</tr>
		<?php endforeach ?>
	<tr>
		<td colspan="3">Total</td>
		<td><?php echo number_format($totalVolume, 2) ?></td>
		<td><?php echo number_format($totalHargaSatuan, 2) ?></td>
		<td><?php echo number_format($totalJumlahHarga, 2) ?></td>
		<td><?php echo number_format($totalBobot, 2) ?></td>
	</tr>
 </table>
 <?php if (Yii::app()->user->checkAccess('engineering')): ?>
		<button type="submit" class="btn btn-primary">Simpan</button> 	
 <?php endif ?>
</form>

