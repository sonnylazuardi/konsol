<?php 
$this->menu=array(
	array('label'=>'Tambah Progress','url'=>array('add')),
);
?>
<?php $this->renderPartial('//project/toolbar'); ?>

<?php $this->renderPartial('laporan'); ?>

<?php $m = explode('-', $minggu) ?>

<?php if(!$print): ?>
	<div class="row">
		<div class="span3">
		<form id="tgl" method="get">
			<?php echo CHtml::dropDownList("minggu", $minggu, Progress::model()->arrMinggu(), array('onchange'=>'$("#tgl").submit()')); ?>
		</form>
		</div>
		<div class="span3">
		<form id="print" method="get">
			<input type="hidden" name="minggu" value="<?php echo $minggu ?>">
			<button type="submit" class="btn btn-primary" name="print" value="1">Print</button>
		</form>
		</div>
	</div>
<?php else: ?>
	<p>Minggu <?php echo $minggu ?></p>
	<img src="themes/bootstrap/img/logo.png" alt="">
<?php endif; ?>

<?php if ($minggu != 0): ?>
<table class="table table-striped table-bordered">
	<tr>
		<td class="thead" colspan="5">Laporan Kemajuan Pekerjaan</td>
	</tr>
	<tr>
		<td class="thead" colspan="5">Minggu ke-<?php echo $m[0] ?>, Bulan <?php echo $m[1] ?></td>
	</tr>
	<tr>
		<td class="thead" colspan="5"><?php echo Project::model()->findByPk(Yii::app()->session['project'])->title ?></td>
	</tr>
	<tr>
		<td>No</td>
		<td>Uraian</td>
		<td>Minggu Lalu (%)</td>
		<td>Minggu Ini (%)</td>
		<td>S/D Minggu Ini (%)</td>
	</tr>
	<tr>
		<td>1</td>
		<td>Aktual</td>
		<td><?php echo number_format($arrAktual[$m[0]-1],3) ?></td>
		<td><?php echo number_format($arrAktual[$m[0]],3) ?></td>
		<td><?php echo number_format($arrAktual[$m[0]+1],3) ?></td>
	</tr>
	<tr>
		<td>2</td>
		<td>Rencana</td>
		<td><?php echo number_format($arrRencana[$m[0]-1],3) ?></td>
		<td><?php echo number_format($arrRencana[$m[0]],3) ?></td>
		<td><?php echo number_format($arrRencana[$m[0]+1],3) ?></td>
	</tr>
	<tr>
		<td>3</td>
		<td>Selisih</td>
		<td><?php echo number_format($arrAktual[$m[0]-1] - $arrRencana[$m[0]-1],3) ?></td>
		<td><?php echo number_format($arrAktual[$m[0]] - $arrRencana[$m[0]],3) ?></td>
		<td><?php echo number_format($arrAktual[$m[0]+1] - $arrRencana[$m[0]+1],3) ?></td>
	</tr>
	<!-- <tr>
		<td colspan="2"></td>
		<td colspan="3">Rencana S/D Minggu Depan</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td colspan="3">XX %</td>
	</tr> -->
	<!-- <tr>
		<td colspan="5">
			Jumlah Tenaga Kerja/Hari (rata-rata seminggu)
		</td>
	</tr>
	<tr>
		<td colspan="3">Tenaga</td>
		<td colspan="2">: 9 orang</td>
	</tr>
	<tr>
		<td colspan="5"></td>
	</tr>
	<tr>
		<td colspan="3">Nilai Konstruksi</td>
		<td colspan="2">: Rp. XXXX</td>
	</tr>
	<tr>
		<td colspan="3">Penyerapan Anggaran TA 2010</td>
		<td colspan="2">: Rp. XXXX</td>
	</tr>
	<tr>
		<td colspan="3">Penyerapan Anggaran TA 2011</td>
		<td colspan="2">: Rp. XXXX</td>
	</tr>
	<tr>
		<td colspan="3">Jangka Waktu pelaksanaan</td>
		<td colspan="2">: 392 hari kalender</td>
	</tr>
	<tr>
		<td colspan="3">Hari ke</td>
		<td colspan="2">: 119</td>
	</tr>
	<tr>
		<td colspan="3">Sisa Hari</td>
		<td colspan="2">: XX hari kalender</td>
	</tr> -->
</table>
<table class="table table-striped table-bordered">
	<tr>
		<td class="thead">Permasalahan</td>
		<td class="thead">Rekomendasi/Penyelesaian</td>
	</tr>
	<tr>
		<td>Perubahan Pekerjaan</td>
		<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, maiores, mollitia, exercitationem, rem nam quo porro facere error odit ipsum incidunt reiciendis saepe sunt laudantium deserunt nemo libero omnis sequi.</td>
	</tr>
	<tr>
		<td>Railing Tangga Darurat</td>
		<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, officia iste doloribus vitae voluptatem vel ex nesciunt adipisci quae voluptate deleniti nemo ullam quidem optio nobis mollitia consectetur totam ut.</td>
	</tr>
	<tr>
		<td>Pekerjaan M.E</td>
		<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, non molestias magni ex aliquam corporis doloribus sed ullam fugiat natus nobis incidunt voluptatibus provident sapiente accusamus animi vitae nihil excepturi?</td>
	</tr>
</table>

<?php endif; ?>