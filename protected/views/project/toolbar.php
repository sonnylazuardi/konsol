<?php
$id = Yii::app()->session['project'];
$this->widget('bootstrap.widgets.TbNavbar',array(
		'fixed'=>false,
		'brand'=>false,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/project/view/id/'.$id)),
                array('label'=>'Profile Project', 'items'=>array(
                	array('label'=>'Gambaran Umum', 'url'=>array('/project/gambaran/id/'.$id)),
                	array('label'=>'Struktur Organisasi', 'url'=>array('/project/structure/id/'.$id)),
                    array('label'=>'Edit Profil', 'url'=>array('/project/update', 'id'=>$id)),
                )),
                array('label'=>'Project', 'items'=>array(
                	array('label'=>'Rencana Progress', 'url'=>array('/progress/manage')),
                	array('label'=>'Drawing', 'url'=>array('/drawing/index', 'id'=>$id)),
                	array('label'=>'Photo', 'url'=>array('/photo/index', 'id'=>$id)),
                	array('label'=>'Video', 'url'=>array('/video/index', 'id'=>$id)),
                    array('label'=>'Camera', 'url'=>array('/project/camera', 'id'=>$id)),
                )),
                array('label'=>'Participant', 'items'=>array(
                	array('label'=>'Manpower', 'url'=>array('/manpower/manage'),'visible'=>Yii::app()->user->checkAccess('manpower')),
                	array('label'=>'Material', 'url'=>array('/material/manage'),'visible'=>Yii::app()->user->checkAccess('material')),
                	array('label'=>'Engineering', 'url'=>array('/progress/harian'),'visible'=>Yii::app()->user->checkAccess('engineering')),
                	array('label'=>'Equipment', 'url'=>array('/equipment/manage'),'visible'=>Yii::app()->user->checkAccess('equipment')),
                	array('label'=>'Keuangan', 'url'=>array('/keuangan/index'),'visible'=>Yii::app()->user->checkAccess('keuangan')),
                )),
                array('label'=>'General Report', 'items'=>array(
                    array('label'=>'Grafik Batang', 'url'=>array('/progress/batang')),
                    array('label'=>'Kurva', 'url'=>array('/progress/kurva')),
                    array('label'=>'Tabel', 'url'=>array('/progress/tabel')),
                )),
                array('label'=>'Chat', 'items'=>array(
                    array('label'=>'Text Chatting', 'url'=>array('/chat/index')),
                    array('label'=>'Video Conference', 'url'=>array('/chat/conference')),
                )),
            ),
        ),
    ),
)); ?>