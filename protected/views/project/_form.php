<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'project-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span6','maxlength'=>128)); ?>

	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->textAreaRow($model,'structure',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->textAreaRow($model,'gambaran',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->textFieldRow($model,'type',array('class'=>'span6','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'budget',array('class'=>'span6','maxlength'=>128)); ?>


	<?php echo $form->textFieldRow($model,'pic',array('id'=>'pics', 'class'=>'span6','maxlength'=>128)); ?>
	<a href="#" class="btn btn-primary" onclick="openKCFinder_singleFile()">Browse Server</a>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('//layouts/ckeditor') ?>
<?php $this->renderPartial('//layouts/kcfinder') ?>