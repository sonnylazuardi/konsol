<?php $this->renderPartial('toolbar', array('model'=>$model)); ?>

<h1>Camera</h1>

<div id="camera"></div>

<?php 
	$cs = Yii::app()->getClientScript();
	$cs->registerCoreScript('jquery');
	$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.webcam.js', CClientScript::POS_HEAD);
 ?>

<script type="text/javascript">
	$("#camera").webcam({
		width: 720,
		height: 480,
		mode: "callback",
		swffile: "<?php echo Yii::app()->baseUrl ?>/swf/jscam_canvas_only.swf",
		onTick: function() {},
		onSave: function() {},
		onCapture: function() {},
		debug: function() {},
		onLoad: function() {}
	});
</script>
