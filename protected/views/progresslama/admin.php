<?php
$this->breadcrumbs=array(
	'Progresses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Progress','url'=>array('index')),
	array('label'=>'Create Progress','url'=>array('create', 'id'=>$id)),
	array('label'=>'Laporan Harian','url'=>array('reportHarian', 'id'=>$id)),
	array('label'=>'Laporan Mingguan','url'=>array('reportMingguan', 'id'=>$id)),
	array('label'=>'Laporan Bulanan','url'=>array('reportBulanan', 'id'=>$id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('progress-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Progress</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'progress-grid',
	'dataProvider'=>$model->search($id),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		'project_id',
		'item_pekerjaan_id',
		array(
			'name'=>'pekerjaan_dilaksanakan',
			'type'=>'raw',
		),
		'tanggal',
		'timestamp',
		'volume',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
