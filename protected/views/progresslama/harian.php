<h1>Laporan Harian</h1>
<p>Tanggal 
	<?php if(!$print): ?>
		<form id="tgl" method="get">
			<?php echo CHtml::dropDownList("tanggal", $tanggal, Progress::model()->arrTanggal(), array('onchange'=>'$("#tgl").submit()')); ?>
		</form>
		<form id="print" method="get">
			<input type="hidden" name="tanggal" value="<?php echo $tanggal ?>">
			<button type="submit" class="btn btn-primary" name="print" value="1">Print</button>
		</form>
	<?php else: ?>
		<?php echo $tanggal ?>
	<?php endif; ?>
</p>

<img src="themes/bootstrap/img/logo.png" alt="">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'progress-grid',
	'template'=>'{items}',
	'dataProvider'=>$model->harian($id, $tanggal),
	'columns'=>array(
		'tanggal',
		array(
			'header' => 'Jenis Pekerjaan',
			'value' => '$data->itemPekerjaan->deskripsi_pekerjaan'
		),
		array(
			'name' => 'pekerjaan_dilaksanakan',
			'type' => 'raw'
		),
		'volume',
		array(
			'header'=>'Jumlah TK',
			'value'=>'$data->jumlahTK',
		),
	),
)); ?>
