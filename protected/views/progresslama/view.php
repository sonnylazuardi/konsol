<?php
$this->breadcrumbs=array(
	'Progresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Progress','url'=>array('index')),
	array('label'=>'Create Progress','url'=>array('create')),
	array('label'=>'Update Progress','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Progress','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Progress','url'=>array('admin')),
);
?>

<h1>View Progress #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'item_pekerjaan_id',
		'pekerjaan_dilaksanakan',
		'tanggal',
		'timestamp',
		'volume',
	),
)); ?>
