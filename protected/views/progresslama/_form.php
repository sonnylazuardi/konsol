<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'progress-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'item_pekerjaan_id',CHtml::listData(ItemPekerjaan::model()->findAll(), 'id', 'deskripsi_pekerjaan'),array('class'=>'span6')); ?>

	<?php echo $form->textAreaRow($model,'pekerjaan_dilaksanakan',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php // echo $form->textFieldRow($model,'tanggal',array('class'=>'span6')); ?>

	<?php // echo $form->textFieldRow($model,'timestamp',array('class'=>'span6')); ?>

	<?php echo $form->textFieldRow($model,'volume',array('class'=>'span6')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php 
	$this->widget('ext.editor.editorku', array(
		'filespath' => Yii::app()->basePath."/../upload/images/ckk/",
		'filesurl' => Yii::app()->baseUrl."/upload/images/ckk/",
		'boleh' => !Yii::app()->user->IsGuest,
	));
 ?>
