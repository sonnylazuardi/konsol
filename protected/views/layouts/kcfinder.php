<script type="text/javascript">
	function openKCFinder_singleFile() {
	    window.KCFinder = {};
	    window.KCFinder.callBack = function(url) {
	        // Actions with url parameter here
	        $('#pics').val(url);
	        $('#imgtags').html('<img src="'+url+'"width="100px"/>');
	        window.KCFinder = null;
	    };
	    window.open('<?php echo Yii::app()->baseUrl ?>/../editor/kcfinder/browse.php?type=images', 'kcfinder_single');
	}
</script>