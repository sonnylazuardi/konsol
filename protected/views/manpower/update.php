<?php
$this->breadcrumbs=array(
	'Manpowers'=>array('index'),
);

$this->menu=array(
	array('label'=>'Manpower','url'=>array('manage')),
);
?>

<h1>Update Manpower <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<?php echo CHtml::link('Hapus', array('delete', 'id'=>$model->id), array('class'=>'btn btn-danger', 'onclick'=>'if(!confirm("Yakin mau dihapus?"))return false')) ?>