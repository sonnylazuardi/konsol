<?php
$this->breadcrumbs=array(
	'Manpowers'=>array('index'),
);

$this->menu=array(
	array('label'=>'List Manpower','url'=>array('index')),
	array('label'=>'Create Manpower','url'=>array('create')),
	array('label'=>'Update Manpower','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Manpower','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manpower','url'=>array('admin')),
);
?>

<h1>View Manpower #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'profession',
		'user_id',
		'area',
		'status',
		'email',
		'home_address',
		'home_city',
		'country',
		'postal_code',
		'work_phone',
		'home_phone',
		'mobile_phone',
		'fax',
		'pic',
		'cost_per_hour',
	),
)); ?>
