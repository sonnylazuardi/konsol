<?php $this->renderPartial('//project/toolbar'); ?>
<h1>Kuangan</h1>
<form id="tgl" method="get">
	<?php echo CHtml::dropDownList("bulan", $bulan, Progress::model()->arrBulan(), array('onchange'=>'$("#tgl").submit()')); ?>
</form>
<table class="table table-bordered">
	<tr>
		<td>Keuangan</td>
		<td>Jumlah</td>
	</tr>
	<tr>
		<td>Manpower</td>
		<td><?php echo number_format($arrBiaya[0],2) ?></td>
	</tr>
	<tr>
		<td>Material</td>
		<td><?php echo number_format($arrBiaya[1],2) ?></td>
	</tr>
	<tr>
		<td>Engineering</td>
		<td><?php echo number_format($arrBiaya[2],2) ?></td>
	</tr>
	<tr>
		<td>Equipment</td>
		<td><?php echo number_format($arrBiaya[3],2) ?></td>
	</tr>
	<tr>
		<td>Total</td>
		<td><?php echo number_format(array_sum($arrBiaya),2) ?></td>
	</tr>
</table>