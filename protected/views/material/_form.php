<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'material-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'project_id',array('class'=>'span6')); ?>

	<?php echo $form->textFieldRow($model,'deskripsi_material',array('class'=>'span6','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'satuan',array('class'=>'span6','maxlength'=>10)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->textFieldRow($model,'durasi',array('class'=>'span6')); ?>

	<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span6')); ?>

	<?php echo $form->textFieldRow($model,'harga_satuan',array('class'=>'span6')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('//layouts/ckeditor') ?>