<?php
$this->menu=array(
	array('label'=>'Create Material','url'=>array('create')),
);
?>
<?php $this->renderPartial('//project/toolbar'); ?>

<h1>Material</h1>

<form method="post">
 <table class="table table-striped table-bordered">
 	<tr>
 		<td rowspan="3">No</td>
		<td rowspan="3">Nama Material</td>
		<td rowspan="3">Satuan</td>
		
		<td colspan="4">Uraian</td>
		<td colspan="19">Tahun 2010</td>
 	</tr>
	<tr>
		<td rowspan="2">Jumlah</td>
		<td rowspan="2">Volume</td>
		<td rowspan="2">Harga Satuan (hari)</td>
		<td rowspan="2">Total Harga</td>
		<td>Juli</td>
		<td colspan="4">Agustus</td>
		<td colspan="4">September</td>
		<td colspan="4">Oktober</td>
		<td colspan="4">November</td>
		<td colspan="2">Desember</td>
	</tr>
	<tr>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
		<td>III</td>
		<td>IV</td>

		<td>I</td>
		<td>II</td>
	</tr>
		<?php 
			$totalHargaSatuan = 0;
			$totalJumlahHarga = 0; 
			//hitung jumlah awal
			foreach ($model as $item) {
				$totalHargaSatuan += $item->harga_satuan;
				$totalJumlahHarga += $item->harga_satuan * $item->jumlah;	
			}
			$c = 1;
		?>
		<?php foreach ($model as $item): ?>
		<tr>
			<td><?php echo $c++ ?></td>
			<td><?php echo CHtml::link($item->deskripsi_material, array('update', 'id'=>$item->id)) ?></td>
			<td><?php echo $item->satuan ?></td>
			<td><?php echo $item->jumlah ?></td>
			<td><?php echo $item->durasi ?></td>
			<td><?php echo number_format($item->harga_satuan, 2) ?></td>
			<td><?php echo number_format($item->harga_satuan * $item->jumlah,2) ?></td>
			<?php 
				$j = 4;
				for ($i=7; $i <= 12; $i++) {
					while($j <= 4)
					{
						echo '<td>'.CHtml::checkBox('placed['.$item->id.']['.$i.']['.$j.']', in_array($item->id.'-'.$i.'-'.$j,$arrPlaced)).'</td>';
						$j++;
						if ($i == 12 && $j == 3) break;
					}				
					$j = 1;
				}
			?>
		</tr>
		<?php endforeach ?>
		<tr>
			<td colspan="5">Total</td>
			<td><?php echo number_format($totalHargaSatuan, 2) ?></td>
			<td><?php echo number_format($totalJumlahHarga, 2) ?></td>
		</tr>
 </table>
<button type="submit" class="btn btn-primary">Simpan</button>
</form>

