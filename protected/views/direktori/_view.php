<div class="span3">
	<div class="well">

	<h4><?php echo CHtml::link($data->nama, array('view', 'id'=>$data->id)); ?></h4>

	<b><?php echo CHtml::encode($data->getAttributeLabel('jabatan')); ?>:</b>
	<?php echo CHtml::encode($data->jabatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telepon')); ?>:</b>
	<?php echo CHtml::encode($data->no_telepon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<?php if (Yii::app()->user->checkAccess('admin')): ?>
		<hr>
		<?php echo CHtml::link('Edit', array('update', 'id'=>$data->id), array('class'=>'btn')) ?> 
		<?php echo CHtml::link('Hapus', array('delete', 'id'=>$data->id), array('class'=>'btn')) ?>
	<?php endif ?>
	</div>
</div>