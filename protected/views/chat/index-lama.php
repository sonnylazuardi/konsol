<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'chat-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->textAreaRow($model,'message',array('rows'=>6, 'cols'=>50, 'class'=>'span8', 'id'=>'msg')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Chat',
	)); ?>
</div>

<?php $this->endWidget(); ?>

<div id="lines" class="well">
	<?php 
		foreach ($chats as $chat) {
			echo "<blockquote>";
			echo $chat->message;
			echo '<small> By '.$chat->user->username.'</small>';
			echo "</blockquote>";
		}
	?>
</div>

<!-- <script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.js"></script> -->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/eventsource.js"></script>

<script>
$(function(){
	var data = new EventSource('<?php echo Yii::app()->createUrl("index.php/chat/send"); ?>');
	$('#chat-form').submit(function(e){
		e.preventDefault();
		$.ajax({
			url: '<?php echo Yii::app()->createUrl("chat/receive"); ?>',
			type: 'POST',
			data: {
				message: $('#msg').val(),
			},
			success: function(){
				$('#msg').val("");
			}
		});
	});
	data.addEventListener('message',function(e){
		var data = $.parseJSON(e.data);
		$('#lines').prepend($('<blockquote>',{text:data.message}).append($('<small>',{text:' By '+data.name})));
	},false);
});
</script>

<?php 
$this->widget('ext.editor.editorku', array(
	'filespath' => Yii::app()->basePath."/../upload/images/ckk/",
	'filesurl' => Yii::app()->baseUrl."/upload/images/ckk/",
	'boleh' => !Yii::app()->user->IsGuest,
));
?>