<div class="span3">

	<?php $this->widget('ext.Yiitube', array('v' => $data->video, 'size'=>'small')); ?>

	<b><?php echo CHtml::link(CHtml::encode($data->name), array('/video/view', 'id'=>$data->id)); ?></b> <br />
	<?php echo CHtml::encode($data->type); ?> <br />
	<?php echo CHtml::encode($data->start_time); ?> <br />
	<?php echo CHtml::encode($data->finish_time); ?> <br />
	<?php echo $data->detail; ?> <br />
</div>