<?php
$this->breadcrumbs=array(
	'Equipments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Equipment','url'=>array('index')),
	array('label'=>'Create Equipment','url'=>array('create')),
	array('label'=>'Update Equipment','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Equipment','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Equipment','url'=>array('admin')),
);
?>

<h1>View Equipment #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'deskripsi_equipment',
		'satuan',
		'keterangan',
		'durasi',
		'jumlah',
		'harga_satuan',
	),
)); ?>
