<?php
$this->menu=array(
	array('label'=>'Create Drawing','url'=>array('create','id'=>$id)),
	array('label'=>'Manage Drawing','url'=>array('admin')),
);
?>

<?php $this->renderPartial('//project/toolbar', array('model'=>Project::model()->findByPk($id))); ?>

<h1>Drawings</h1>

<div class="row">
<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</div>