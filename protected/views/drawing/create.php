<?php
$this->breadcrumbs=array(
	'Drawings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Drawing','url'=>array('index')),
	array('label'=>'Manage Drawing','url'=>array('admin')),
);
?>

<h1>Create Drawing</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>