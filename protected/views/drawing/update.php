<?php
$this->breadcrumbs=array(
	'Drawings'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Drawing','url'=>array('index')),
	array('label'=>'Create Drawing','url'=>array('create')),
	array('label'=>'View Drawing','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Drawing','url'=>array('admin')),
);
?>

<h1>Update Drawing <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>