<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'drawing-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span6','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'type',array('class'=>'span6','maxlength'=>128)); ?>

	<?php echo $form->textAreaRow($model,'detail',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor span6')); ?>

	<?php echo $form->textFieldRow($model,'pic',array('class'=>'span6','maxlength'=>128, 'id'=>'pics')); ?>
	<a href="#" class="btn btn-primary" onclick="openKCFinder_singleFile()">Browse Server</a>

	<?php echo $form->textFieldRow($model,'date',array('class'=>'span6')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('//layouts/ckeditor') ?>
<?php $this->renderPartial('//layouts/kcfinder') ?>