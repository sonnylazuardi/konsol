<?php
$this->menu=array(
	array('label'=>'Create Photo','url'=>array('create', 'id'=>$id)),
	array('label'=>'Manage Photo','url'=>array('admin')),
);
?>

<?php $this->renderPartial('//project/toolbar', array('model'=>Project::model()->findByPk($id))); ?>

<h1>Photos</h1>

<div class="row">
<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</div>