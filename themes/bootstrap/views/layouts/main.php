<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>
	<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<div class="container">
	<div id="logo"></div>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
		'type'=>'inverse',
		'fixed'=>false,
		'brand'=>false,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'Profile', 'items'=>array(
                	array('label'=>'Tentang Perusahaan', 'url'=>array('/page/show/id/profile')),
                	array('label'=>'Sejarah Perusahaan', 'url'=>array('/page/show/id/sejarah')),
                	array('label'=>'Visi Misi', 'url'=>array('/page/show/id/visimisi')),
                	array('label'=>'Struktur Organisasi', 'url'=>array('/page/show/id/struktur')),
                )),
                array('label'=>'Project', 'url'=>array('/project/index'), 'items'=>Project::model()->getAllProject()),
                array('label'=>'Forum', 'url'=>array('/forum')),
                array('label'=>'E-procurement', 'url'=>array('/procurement/index')),
                array('label'=>'Contact', 'url'=>array('/site/contact')),
                array('label'=>'Direktori', 'url'=>array('/direktori/index'), 'visible'=>!Yii::app()->user->isGuest),
            ),
        ),
        array(
        	'class'=>'bootstrap.widgets.TbMenu',
        	'htmlOptions'=>array('class'=>'pull-right'),
          'items'=>array(
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>Yii::app()->user->name, 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
							array('label'=>'Project', 'url'=>array('/project/index')),	
							array('label'=>'Page', 'url'=>array('/page/admin')),	
							array('label'=>'Logout', 'url'=>array('/site/logout')),	
						)),
					),
				),
    ),
)); ?>
</div>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<hr>
		Copyright &copy; <?php echo date('Y'); ?> by Konsol Team.<br/>
		All Rights Reserved.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
