-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2013 at 06:57 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `konsol`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktual_progress`
--

CREATE TABLE IF NOT EXISTS `aktual_progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `progress_id` int(11) NOT NULL,
  `pekerjaan_dilaksanakan` text NOT NULL,
  `tanggal` date NOT NULL,
  `timestamp` datetime NOT NULL,
  `volume` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a1` (`progress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `aktual_progress`
--

INSERT INTO `aktual_progress` (`id`, `progress_id`, `pekerjaan_dilaksanakan`, `tanggal`, `timestamp`, `volume`) VALUES
(1, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 1', '2013-09-01', '2013-05-11 12:06:00', 2),
(2, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 1', '2013-10-01', '2013-05-11 12:06:00', 2),
(3, 23, 'Pabrikasi Pemasangan Besi dan Balok Lantai 1', '2013-11-01', '2013-05-11 12:06:00', 2),
(4, 23, 'Pabrikasi Pemasangan Besi dan Balok Lantai 2', '2013-09-01', '2013-05-11 12:06:00', 2),
(5, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 3', '2013-10-01', '2013-05-11 12:06:00', 2),
(12, 25, 'Pabrikasi Pemasangan Besi dan Balok Lantai 10', '2013-10-01', '2013-05-11 12:06:00', 2),
(13, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 11', '2013-09-01', '2013-05-11 12:06:00', 2),
(14, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 12', '2013-10-01', '2013-05-11 12:06:00', 2),
(15, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 13', '2013-08-01', '2013-05-11 12:06:00', 2),
(16, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 14', '2013-08-01', '2013-05-11 12:06:00', 2),
(17, 24, 'Pabrikasi Pemasangan Besi dan Balok Lantai 15', '2013-08-01', '2013-05-11 12:06:00', 2),
(18, 24, 'sdsdfdf', '2013-11-01', '2013-05-11 12:06:00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `direktori`
--

CREATE TABLE IF NOT EXISTS `direktori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  `jabatan` varchar(128) DEFAULT NULL,
  `no_telepon` int(11) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `direktori`
--

INSERT INTO `direktori` (`id`, `nama`, `jabatan`, `no_telepon`, `alamat`) VALUES
(1, 'Budi', 'Manager Executive', 2147483647, 'Jalan budiman budiluhur 5 Bandung'),
(2, 'Rumah Sakit Borromeus', '', 2147483647, 'Jalan Dago Bandung'),
(3, 'Jhony', 'Pekerja', 812321332, 'Jalan braga no 7');

-- --------------------------------------------------------

--
-- Table structure for table `drawing`
--

CREATE TABLE IF NOT EXISTS `drawing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `type` varchar(128) DEFAULT NULL,
  `detail` text,
  `date` datetime NOT NULL,
  `pic` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `photo_project` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `drawing`
--

INSERT INTO `drawing` (`id`, `project_id`, `name`, `type`, `detail`, `date`, `pic`) VALUES
(1, 2, 'hello', 'hello', '<p>hello</p>\r\n', '2013-05-10 00:00:00', '/konsol/upload/images/ckk/images/Picture1(1).png');

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `deskripsi_equipment` varchar(255) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `durasi` int(5) NOT NULL,
  `jumlah` float(11,0) NOT NULL,
  `harga_satuan` float(11,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `material_project` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id`, `project_id`, `deskripsi_equipment`, `satuan`, `keterangan`, `durasi`, `jumlah`, `harga_satuan`) VALUES
(1, 2, 'Excavator', 'Unit', '', 12, 2, 900000),
(2, 2, 'Dump Truck', 'Unit', '', 12, 6, 600000),
(3, 2, 'Generator Set', 'Unit', '', 15, 1, 600000),
(4, 2, 'Mesin Las', 'Unit', '', 16, 2, 300000),
(5, 2, 'Tug Boat', 'Unit', '', 7, 2, 1500000),
(6, 2, 'Ponton', 'Unit', '', 7, 2, 800000),
(7, 2, 'Crawler Crane 35 ton', 'Unit', '', 11, 2, 800000),
(8, 2, 'Crawler Crane 80 ton', 'Unit', '', 11, 1, 850000),
(9, 2, 'Theodolith', 'Unit', '', 19, 2, 300000),
(10, 2, 'Waterpass', 'Unit', '', 19, 2, 300000),
(11, 2, 'Bar Bender', 'Unit', '', 14, 1, 300000),
(12, 2, 'Bar Cutter', 'Unit', '', 14, 1, 300000),
(13, 2, 'Auger', 'Unit', '', 8, 1, 700000),
(14, 3, 'Excavator', 'Unit', '', 12, 2, 900000),
(15, 3, 'Dump Truck', 'Unit', '', 12, 6, 600000),
(16, 3, 'Generator Set', 'Unit', '', 15, 1, 600000),
(17, 3, 'Mesin Las', 'Unit', '', 16, 2, 300000),
(18, 3, 'Tug Boat', 'Unit', '', 7, 2, 1500000),
(19, 3, 'Ponton', 'Unit', '', 7, 2, 800000),
(20, 3, 'Crawler Crane 35 ton', 'Unit', '', 11, 2, 800000),
(21, 3, 'Crawler Crane 80 ton', 'Unit', '', 11, 1, 850000),
(22, 3, 'Theodolith', 'Unit', '', 19, 2, 300000),
(23, 3, 'Waterpass', 'Unit', '', 19, 2, 300000),
(24, 3, 'Bar Bender', 'Unit', '', 14, 1, 300000),
(25, 3, 'Bar Cutter', 'Unit', '', 14, 1, 300000),
(26, 3, 'Auger', 'Unit', '', 8, 1, 700000),
(27, 4, 'Excavator', 'Unit', '', 12, 2, 900000),
(28, 4, 'Dump Truck', 'Unit', '', 12, 6, 600000),
(29, 4, 'Generator Set', 'Unit', '', 15, 1, 600000),
(30, 4, 'Mesin Las', 'Unit', '', 16, 2, 300000),
(31, 4, 'Tug Boat', 'Unit', '', 7, 2, 1500000),
(32, 4, 'Ponton', 'Unit', '', 7, 2, 800000),
(33, 4, 'Crawler Crane 35 ton', 'Unit', '', 11, 2, 800000),
(34, 4, 'Crawler Crane 80 ton', 'Unit', '', 11, 1, 850000),
(35, 4, 'Theodolith', 'Unit', '', 19, 2, 300000),
(36, 4, 'Waterpass', 'Unit', '', 19, 2, 300000),
(37, 4, 'Bar Bender', 'Unit', '', 14, 1, 300000),
(38, 4, 'Bar Cutter', 'Unit', '', 14, 1, 300000),
(39, 4, 'Auger', 'Unit', '', 8, 1, 700000);

-- --------------------------------------------------------

--
-- Table structure for table `external`
--

CREATE TABLE IF NOT EXISTS `external` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procurement_id` int(11) NOT NULL,
  `nama_vendor` varchar(128) NOT NULL,
  `alamat_vendor` varchar(128) NOT NULL,
  `nama_pic` varchar(128) NOT NULL,
  `tanggal_berdiri` date NOT NULL,
  `pengalaman` text NOT NULL,
  `telepon` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `harga_penawaran` int(11) NOT NULL,
  `gambar` varchar(128) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_procurement` (`procurement_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `external`
--

INSERT INTO `external` (`id`, `procurement_id`, `nama_vendor`, `alamat_vendor`, `nama_pic`, `tanggal_berdiri`, `pengalaman`, `telepon`, `email`, `harga_penawaran`, `gambar`, `timestamp`) VALUES
(1, 1, 'Sonny', 'jalan di langit', 'sonny', '0000-00-00', '<p>berpengalama</p>\r\n', 821123, 'sonny@sonny', 51000000, 'asdasdsad.jpg', '2013-06-07 07:31:47'),
(2, 1, 'tedsting', 'tedsting', 'tedsting', '0000-00-00', '<p>tedsting</p>\r\n', 123213213, 'tedsting@tedsting.com', 52000000, 'tedsting', '2013-06-07 07:35:38'),
(3, 1, 'joetaslim', 'joetaslim', 'joetaslim', '0000-00-00', '<p>joetaslim</p>\r\n', 12341242, 'joetaslim@joetaslim.com', 2147483647, '', '2013-06-07 08:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `listorder` smallint(5) unsigned NOT NULL,
  `is_locked` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_forum_forum` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `forum`
--

INSERT INTO `forum` (`id`, `parent_id`, `title`, `description`, `listorder`, `is_locked`) VALUES
(1, NULL, 'Announcements', 'Announcements', 0, 1),
(2, 1, 'New releases', 'Announcements about new releases', 10, 0),
(3, NULL, 'Support', '', 20, 0),
(4, 3, 'Installation and configuration', 'Problems with installation and/or configuration, incompatibility issues, etc.', 10, 0),
(5, 3, 'Bugs', 'Things not working at all, or not as they should', 20, 0),
(6, 3, 'Missing features', 'Fetures you think should be included in a future release', 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `forumuser`
--

CREATE TABLE IF NOT EXISTS `forumuser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `firstseen` datetime DEFAULT NULL,
  `lastseen` datetime DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `siteid` (`siteid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `forumuser`
--

INSERT INTO `forumuser` (`id`, `siteid`, `name`, `firstseen`, `lastseen`, `signature`) VALUES
(1, 'admin', 'admin', NULL, NULL, NULL),
(2, 'demo', 'demo', NULL, NULL, NULL),
(3, '1', 'test', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `manpower`
--

CREATE TABLE IF NOT EXISTS `manpower` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `deskripsi_manpower` varchar(255) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `durasi` int(5) NOT NULL,
  `jumlah` float(11,0) NOT NULL,
  `harga_satuan` float(11,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `material_project` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `manpower`
--

INSERT INTO `manpower` (`id`, `project_id`, `deskripsi_manpower`, `satuan`, `keterangan`, `durasi`, `jumlah`, `harga_satuan`) VALUES
(1, 2, 'Mandor lapangan', 'orang', '', 18, 3, 100000),
(2, 2, 'Tenaga gali', 'orang', '', 12, 6, 65000),
(3, 2, 'Tenaga bor', 'orang', '', 15, 6, 65000),
(4, 2, 'Operator excavator', 'orang', '', 16, 2, 80000),
(5, 2, 'Operator Tug Boat', 'orang', '', 7, 2, 90000),
(6, 2, 'Operator Auger', 'orang', '', 7, 2, 90000),
(7, 2, 'Operator Crawler Crane 35 ton', 'orang', '', 11, 2, 90000),
(8, 2, 'Operator Crawler Crane 80 ton', 'orang', '', 11, 2, 90000),
(9, 2, 'Tenaga pengukur', 'orang', '', 19, 2, 80000),
(10, 2, 'Supir', 'orang', '', 19, 6, 70000),
(11, 2, 'Tukang besi', 'orang', '', 14, 8, 65000),
(12, 2, 'Tukang Cor', 'orang', '', 14, 8, 65000),
(13, 2, 'Tukang Bekisting', 'orang', '', 8, 6, 65000),
(14, 3, 'Mandor lapangan', 'orang', '', 18, 3, 100000),
(15, 3, 'Tenaga gali', 'orang', '', 12, 6, 65000),
(16, 3, 'Tenaga bor', 'orang', '', 15, 6, 65000),
(17, 3, 'Operator excavator', 'orang', '', 16, 2, 80000),
(18, 3, 'Operator Tug Boat', 'orang', '', 7, 2, 90000),
(19, 3, 'Operator Auger', 'orang', '', 7, 2, 90000),
(20, 3, 'Operator Crawler Crane 35 ton', 'orang', '', 11, 2, 90000),
(21, 3, 'Operator Crawler Crane 80 ton', 'orang', '', 11, 2, 90000),
(22, 3, 'Tenaga pengukur', 'orang', '', 19, 2, 80000),
(23, 3, 'Supir', 'orang', '', 19, 6, 70000),
(24, 3, 'Tukang besi', 'orang', '', 14, 8, 65000),
(25, 3, 'Tukang Cor', 'orang', '', 14, 8, 65000),
(26, 3, 'Tukang Bekisting', 'orang', '', 8, 6, 65000),
(27, 4, 'Mandor lapangan', 'orang', '', 18, 3, 100000),
(28, 4, 'Tenaga gali', 'orang', '', 12, 6, 65000),
(29, 4, 'Tenaga bor', 'orang', '', 15, 6, 65000),
(30, 4, 'Operator excavator', 'orang', '', 16, 2, 80000),
(31, 4, 'Operator Tug Boat', 'orang', '', 7, 2, 90000),
(32, 4, 'Operator Auger', 'orang', '', 7, 2, 90000),
(33, 4, 'Operator Crawler Crane 35 ton', 'orang', '', 11, 2, 90000),
(34, 4, 'Operator Crawler Crane 80 ton', 'orang', '', 11, 2, 90000),
(35, 4, 'Tenaga pengukur', 'orang', '', 19, 2, 80000),
(36, 4, 'Supir', 'orang', '', 19, 6, 70000),
(37, 4, 'Tukang besi', 'orang', '', 14, 8, 65000),
(38, 4, 'Tukang Cor', 'orang', '', 14, 8, 65000),
(39, 4, 'Tukang Bekisting', 'orang', '', 8, 6, 65000);

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `deskripsi_material` varchar(255) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `durasi` int(5) NOT NULL,
  `jumlah` float(11,2) NOT NULL,
  `harga_satuan` float(11,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `material_project` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `project_id`, `deskripsi_material`, `satuan`, `keterangan`, `durasi`, `jumlah`, `harga_satuan`) VALUES
(1, 2, 'Ready Mix Beton K 800', 'm3', '', 1, 1.32, 29445120.00),
(2, 2, 'Ready Mix Beton K 500', 'm3', '', 1, 8.96, 3010533.00),
(3, 2, 'Ready Mix Beton K 350', 'm3', '', 5, 369.03, 1971198.88),
(4, 2, 'Ready Mix Beton K 250', 'm3', '', 8, 707.72, 1746700.88),
(5, 2, 'Ready Mix Beton K 175', 'm3', '', 10, 282.27, 1074439.38),
(6, 2, 'Ready Mix Beton K 125', 'm3', '', 8, 96.29, 755841.62),
(7, 2, 'Baja Tulangan U32 Ulir', 'm3', '', 13, 150479.52, 15637.07),
(8, 2, 'Hangar CT Bar dia. 56 mm furbished', 'm3', '', 2, 1366.11, 184140.00),
(9, 3, 'Ready Mix Beton K 800', 'm3', '', 1, 1.32, 29445120.00),
(10, 3, 'Ready Mix Beton K 500', 'm3', '', 1, 8.96, 3010533.00),
(11, 3, 'Ready Mix Beton K 350', 'm3', '', 5, 369.03, 1971198.88),
(12, 3, 'Ready Mix Beton K 250', 'm3', '', 8, 707.72, 1746700.88),
(13, 3, 'Ready Mix Beton K 175', 'm3', '', 10, 282.27, 1074439.38),
(14, 3, 'Ready Mix Beton K 125', 'm3', '', 8, 96.29, 755841.62),
(15, 3, 'Baja Tulangan U32 Ulir', 'm3', '', 13, 150479.52, 15637.07),
(16, 3, 'Hangar CT Bar dia. 56 mm furbished', 'm3', '', 2, 1366.11, 184140.00),
(17, 4, 'Ready Mix Beton K 800', 'm3', '', 1, 1.32, 29445120.00),
(18, 4, 'Ready Mix Beton K 500', 'm3', '', 1, 8.96, 3010533.00),
(19, 4, 'Ready Mix Beton K 350', 'm3', '', 5, 369.03, 1971198.88),
(20, 4, 'Ready Mix Beton K 250', 'm3', '', 8, 707.72, 1746700.88),
(21, 4, 'Ready Mix Beton K 175', 'm3', '', 10, 282.27, 1074439.38),
(22, 4, 'Ready Mix Beton K 125', 'm3', '', 8, 96.29, 755841.62),
(23, 4, 'Baja Tulangan U32 Ulir', 'm3', '', 13, 150479.52, 15637.07),
(24, 4, 'Hangar CT Bar dia. 56 mm furbished', 'm3', '', 2, 1366.11, 184140.00);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `pkey` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `u3` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `pkey`, `content`, `status`, `create_time`, `update_time`, `user_id`) VALUES
(1, 'About Konsol', 'home', '<p><img alt="" src="/konsol/upload/images/ckk/images/banner_01.jpg" style="width: 910px; height: 210px;" /><img alt="" src="/konsol/upload/images/ckk/images/banner_02.jpg" style="width: 910px; height: 210px;" /></p>\r\n\r\n<p><strong>Konsol&nbsp;</strong>merupakan wadah organisasi perusahaan perusahaan yang bergerak dibidang jasa konstruksi yang dimana meliputi bidang arsitektural, bidang sipil, bidang mekanikal, bidang elektrikal, dan bidang tata lingkungan.</p>\r\n\r\n<p><strong>Konsol </strong>merupakan asosiasi yang telah mendapatkan akreditasi &quot;A&quot; melalui Surat Tanda Lulus Akreditasi Asosiasi Perusahaan No. 07/AKR/LPJK/D/II/2003 tanggal 24 Februari 2003 dari LPJKN (Lembaga Pengembangan Jasa Konstruksi Nasional).</p>\r\n', 1, '2013-02-20 12:37:48', '2013-02-24 06:43:36', 1),
(2, 'Sejarah', 'sejarah', '<p>Testing Sejarah</p>\r\n', 1, '2013-02-20 13:04:33', '2013-05-03 18:27:11', 1),
(5, 'Profile', 'profile', '<p><span style="line-height: 1.6em;">Seiring dengan perkembangan jasa konstruksi di Tanah Air Indonesia ini serta amanat Undang-Undang No. 18 Tahun 1999 tentang Jasa Konstruksi yang menyatakan bahwa Perusahaan Jasa Konstruksi sebagai bagian Masyarakat Jasa Konstruksi dan sekaligus sebagai bagian dari kesatuan masyarakat pelaku ekonomi Indonesia, maka sejak Tanggal 27 Juli 2002, Asosiasi GAPEKSINDO dideklarasikan di Makassar - Sulawesi Selatan, dengan akta notaris Hidayat Aziek, SH Nomor 6 Tanggal 27 Juli 2002.</span></p>\r\n\r\n<p>KONSOL merupakan wadah organisasi perusahaan perusahaan yang bergerak dibidang jasa konstruksi yang dimana meliputi bidang arsitektural, bidang sipil, bidang mekanikal, bidang elektrikal, dan bidang tata lingkungan.</p>\r\n\r\n<p>KONSOL&nbsp;merupakan asosiasi yang telah mendapatkan akreditasi &quot;A&quot; melalui Surat Tanda Lulus Akreditasi Asosiasi Perusahaan No. 07/AKR/LPJK/D/II/2003 tanggal 24 Februari 2003 dari LPJKN (Lembaga Pengembangan Jasa Konstruksi Nasional).</p>\r\n\r\n<p>Pada awal tahun 2002 didirikan hingga tahun 2012 (berjalan), GAPEKSINDO telah melayani anggotanya ~ 22.916 badan usaha yang bergerak dibidang jasa konstruksi yang tersebar pada 450 kantor daerah/cabang diseluruh propinsi di Indonesia, serta pada tanggal 4 November 2008 GAPEKSINDO telah mendapatkan sertifikasi manajemen mutu ISO 9001:2008 dari TUV Nord. GAPEKSINDO pada tanggal 14 Juni 2011, KONSOL termasuk salah satu Asosiasi Perusahaan yang memenuhi persyaratan yang memenuhi kriteria untuk menjadi kelompok unsur lembaga tingkat nasional sesuai dengan Keputusan MENTERI PEKERJAAN UMUM NOMOR : 154/KPTS/M/2011</p>\r\n\r\n<p>Video sekilas KONSOL [klik/youtube]<br />\r\nVideo Hymne KONSOL [klik/youtube]<br />\r\nVideo Mars KONSOL [klik/youtube]</p>\r\n', 1, '2013-05-03 18:12:45', '2013-05-03 18:21:38', 1),
(6, 'Visi & Misi Konsol', 'visimisi', '<p><strong>VISI KONSOL</strong> Terwujudnya oragnisasi yang mandiri dan profesional sebagai wadah berperannya pelakssana konstruksi dalam menjalankan pengabdian usahanya menuju pembangunan bangsa Indonesia yang bermartabat.</p>\r\n\r\n<p><strong>MISI KONSOL</strong> Menghimpun dan mengembangkan perusahaan nasional dibidang usaha pelaksana jasa konstruksi dalam tatanan dunia usaha yang sehat, mampu bersinergi sesama pelaksana jasa konstruksi.</p>\r\n\r\n<p><strong>TUJUAN KONSOL</strong> Menghimpun perusahaan-perusahaan nasional dibidang jasa konstruksi didalam satu wadah.</p>\r\n\r\n<p>Membina dan mengembangkan kemampuan usaha anggota.</p>\r\n\r\n<p>Mendorong terwujudnya tertib pembangunan dan iklim usaha yang sehat.</p>\r\n\r\n<p>Mewujudkan rasa kesetiakawanan sesama anggota dengan menjauhkan diri dari persaingan yang tidak sehat dalam menjalankan usahanya.</p>\r\n\r\n<p>Mendorong komunikasi dan konsultasi, Baik antar anggota maupun anggota dengan pemerintah, anggota dengan perusahaan Nasional/Asing serta Lembaga dan Organisasi lain mengenai hal-hal yang berkaitan dengan jasa Konstruksi.</p>\r\n\r\n<p>Melakukan pembinaan dan pengembangan usaha nasional agar menjadi pengusaha yang kokoh, mandiri, profesional dan berdaya saing tinggi dengan hasil pekerjaan yang berkualitas.</p>\r\n\r\n<p>Melakukan Penyuluhan, bimbingan,bantuan Dan melindungi serta memperjuangkan kepentingan anggota.</p>\r\n', 1, '2013-05-03 18:24:27', '2013-05-03 18:24:27', 1),
(7, 'Struktur Organisasi', 'struktur', '<p>tes struktur</p>\r\n', 1, '2013-05-03 18:27:35', '2013-05-03 18:27:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `type` varchar(128) DEFAULT NULL,
  `detail` text,
  `date` datetime NOT NULL,
  `pic` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `photo_project` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `project_id`, `name`, `type`, `detail`, `date`, `pic`) VALUES
(2, 2, 'test', 'test', '<p>tsest</p>\r\n', '2013-05-10 00:00:00', '/konsol/upload/images/ckk/images/Picture2.jpg'),
(3, 2, 'yuhuu', 'asd', '<p>xsdf</p>\r\n', '2013-05-10 00:00:00', '/konsol/upload/images/ckk/images/banner_01.jpg'),
(4, 2, 'yiihaa', 'asdfas', '<p>sdfsfsdf</p>\r\n', '2013-05-10 00:00:00', '/konsol/upload/images/ckk/images/porto4.jpg'),
(5, 2, 'tess', 'dfasdf', '<p>sdfsdf</p>\r\n', '2013-05-10 00:00:00', '/konsol/upload/images/ckk/images/tes.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `thread_id` int(10) unsigned NOT NULL,
  `editor_id` int(10) unsigned DEFAULT NULL,
  `content` text NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_author` (`author_id`),
  KEY `FK_post_editor` (`editor_id`),
  KEY `FK_post_thread` (`thread_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `author_id`, `thread_id`, `editor_id`, `content`, `created`, `updated`) VALUES
(1, 1, 1, NULL, 'The first release is a fact!', 0, 0),
(2, 2, 2, NULL, 'This obviously can''t be right.', 0, 0),
(3, 2, 3, NULL, 'When posting a new thread, the creation date is set to Jan 1, 1970 01:00:00 AM...', 0, 0),
(4, 2, 3, NULL, 'This should be fixed now!', 2012, 2012),
(5, 2, 3, NULL, 'Oops! Let''s try that again...\r\nThis should be fixed now!', 1349540442, 1349540442),
(6, 2, 4, NULL, 'I believe it shows the the last thread instead...', 1349540563, 1349540563),
(7, 2, 4, NULL, 'Fixed!', 1349561144, 1349561144),
(8, 1, 4, NULL, 'Test reply', 1349563344, 1349563344),
(9, 1, 4, NULL, 'Another test reply, locking thread', 1349563360, 1349563360),
(10, 1, 4, NULL, 'Opps. Locking thread for real now', 1349564868, 1349564868),
(11, 1, 3, NULL, 'Thread locked, maybe', 1349564945, 1349632036),
(12, 1, 5, NULL, 'Allow users to define a signature, and add this to posts by them.', 1349570366, 1349570366),
(13, 1, 6, NULL, 'Add BB code support, and some sort of wysiwyg editor', 1349570413, 1349570413),
(14, 1, 7, NULL, 'Allow attachments to be added to posts\r\n\r\nSome *examples* of **markup**\r\n\r\ninline use of `code` is possible too!\r\n\r\nLet''s see what a\r\n> blockquote looks like\r\nwithin a pargraph\r\n\r\n    [php showLineNumbers=1]\r\n    echo ''It can highlight code too!'';\r\n', 1349578699, 1349578699),
(15, 3, 8, NULL, 'testing', 1368034409, 1368034409),
(16, 3, 9, NULL, 'testing', 1368034435, 1368034435);

-- --------------------------------------------------------

--
-- Table structure for table `procurement`
--

CREATE TABLE IF NOT EXISTS `procurement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` float(11,2) NOT NULL,
  `gambar` varchar(128) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `deadline` datetime NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `procurement`
--

INSERT INTO `procurement` (`id`, `judul`, `deskripsi`, `harga`, `gambar`, `timestamp`, `deadline`, `status`) VALUES
(1, 'Pengadaan Mesin Pengerek', '<p>Dibutuhkan mesin pengerej dengan deskripsi berikut ini</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Bagi yang berminat silakan ambil procurement ini</p>\r\n', 50000000.00, '/konsol/upload/images/ckk/images/Picture1.png', '2013-06-07 05:10:12', '2013-06-07 10:27:19', 1),
(2, 'Butuh Semen Baja', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 2000000.00, '/konsol/upload/images/ckk/images/banner_02.jpg', '2013-06-07 08:21:30', '2013-06-27 10:27:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `progress`
--

CREATE TABLE IF NOT EXISTS `progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `jenis_pekerjaan` varchar(200) DEFAULT NULL,
  `deskripsi_progress` varchar(255) NOT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `volume` float(11,2) NOT NULL,
  `harga_satuan` float(11,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a1` (`jenis_pekerjaan`),
  KEY `pekerjaan_project` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `progress`
--

INSERT INTO `progress` (`id`, `project_id`, `jenis_pekerjaan`, `deskripsi_progress`, `satuan`, `keterangan`, `volume`, `harga_satuan`) VALUES
(1, 2, 'DIVISI 1. UMUM', 'Mobilisasi', 'Ls', '', 1.00, 15535625.00),
(2, 2, 'DIVISI 2. DRAINASE', 'Galian untuk selokan drainase dan saluran air\r\nGalian untuk selokan drainase dan saluran air', 'm3', '', 865.42, 25817.18),
(3, 2, 'DIVISI 3. PEKERJAAN TANAH', 'Galian Struktur dengan kedalaman 0 - 2 m', 'm3', '', 486.52, 38490.93),
(4, 2, 'DIVISI 3. PEKERJAAN TANAH', 'Timbunan biasa dari selain galian sumber bahan', 'm3', '', 1704.68, 66725.94),
(5, 2, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas A', 'm3', '', 0.00, 552118.50),
(6, 2, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas B', 'm3', '', 0.00, 551513.50),
(7, 2, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas C', 'm3', '', 736.63, 259424.12),
(8, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 800', 'm3', '', 1.32, 29445120.00),
(9, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 500', 'm3', '', 8.96, 3010533.00),
(10, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 350', 'm3', '', 369.03, 1971198.88),
(11, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 250', 'm3', '', 707.72, 1746700.88),
(12, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 175', 'm3', '', 282.27, 1074439.38),
(13, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 125', 'm3', '', 96.29, 755841.62),
(14, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Baja Tulangan U32 Ulir', 'Kg', '', 150479.41, 15637.07),
(15, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pabrikasi dan pemasangan baja bangunan tegangan leleh 2500 kg/cm2', 'Kg', '', 4911.93, 49495.79),
(16, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pengadaan lengkap bangunan atas rangka baja pelengkung', 'Kg', '', 476812.56, 33566.50),
(17, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pemasangan jembatan rangka baja', 'Kg', '', 412779.81, 16958.33),
(18, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Hangar CT Bar dia. 56 mm furbished', 'Kg', '', 1366.11, 184140.00),
(19, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Strand stressing', 'Ton', '', 1.90, 55491184.00),
(20, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pondasi Cerucuk, Penyediaan dan Pemancangan', 'm', '', 1980.00, 8030.40),
(21, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pemancangan Sheet Pile', 'm', '', 560.00, 258880.27),
(22, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Tipe Asphaltic Plig', 'm', '', 11.00, 2062708.38),
(23, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Single Seal', 'Buah', '', 1.00, 334950112.00),
(24, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Multi Seal', 'Buah', '', 1.00, 625790080.00),
(25, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pot Bearing', 'Buah', '', 8.00, 320652864.00),
(26, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Sandaran / Railing (Pengadaan)', 'Kg', '', 0.00, 20627.85),
(27, 2, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Sandaran (pemasangan)', 'Kg', '', 19831.20, 13684.37),
(28, 3, 'DIVISI 1. UMUM', 'Mobilisasi', 'Ls', '', 1.00, 15535625.00),
(29, 3, 'DIVISI 2. DRAINASE', 'Galian untuk selokan drainase dan saluran air\r\nGalian untuk selokan drainase dan saluran air', 'm3', '', 865.42, 25817.18),
(30, 3, 'DIVISI 3. PEKERJAAN TANAH', 'Galian Struktur dengan kedalaman 0 - 2 m', 'm3', '', 486.52, 38490.93),
(31, 3, 'DIVISI 3. PEKERJAAN TANAH', 'Timbunan biasa dari selain galian sumber bahan', 'm3', '', 1704.68, 66725.94),
(32, 3, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas A', 'm3', '', 0.00, 552118.50),
(33, 3, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas B', 'm3', '', 0.00, 551513.50),
(34, 3, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas C', 'm3', '', 736.63, 259424.12),
(35, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 800', 'm3', '', 1.32, 29445120.00),
(36, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 500', 'm3', '', 8.96, 3010533.00),
(37, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 350', 'm3', '', 369.03, 1971198.88),
(38, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 250', 'm3', '', 707.72, 1746700.88),
(39, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 175', 'm3', '', 282.27, 1074439.38),
(40, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 125', 'm3', '', 96.29, 755841.62),
(41, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Baja Tulangan U32 Ulir', 'Kg', '', 150479.41, 15637.07),
(42, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pabrikasi dan pemasangan baja bangunan tegangan leleh 2500 kg/cm2', 'Kg', '', 4911.93, 49495.79),
(43, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pengadaan lengkap bangunan atas rangka baja pelengkung', 'Kg', '', 476812.56, 33566.50),
(44, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pemasangan jembatan rangka baja', 'Kg', '', 412779.81, 16958.33),
(45, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Hangar CT Bar dia. 56 mm furbished', 'Kg', '', 1366.11, 184140.00),
(46, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Strand stressing', 'Ton', '', 1.90, 55491184.00),
(47, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pondasi Cerucuk, Penyediaan dan Pemancangan', 'm', '', 1980.00, 8030.40),
(48, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pemancangan Sheet Pile', 'm', '', 560.00, 258880.27),
(49, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Tipe Asphaltic Plig', 'm', '', 11.00, 2062708.38),
(50, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Single Seal', 'Buah', '', 1.00, 334950112.00),
(51, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Multi Seal', 'Buah', '', 1.00, 625790080.00),
(52, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pot Bearing', 'Buah', '', 8.00, 320652864.00),
(53, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Sandaran / Railing (Pengadaan)', 'Kg', '', 0.00, 20627.85),
(54, 3, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Sandaran (pemasangan)', 'Kg', '', 19831.20, 13684.37),
(55, 4, 'DIVISI 1. UMUM', 'Mobilisasi', 'Ls', '', 1.00, 15535625.00),
(56, 4, 'DIVISI 2. DRAINASE', 'Galian untuk selokan drainase dan saluran air\r\nGalian untuk selokan drainase dan saluran air', 'm3', '', 865.42, 25817.18),
(57, 4, 'DIVISI 3. PEKERJAAN TANAH', 'Galian Struktur dengan kedalaman 0 - 2 m', 'm3', '', 486.52, 38490.93),
(58, 4, 'DIVISI 3. PEKERJAAN TANAH', 'Timbunan biasa dari selain galian sumber bahan', 'm3', '', 1704.68, 66725.94),
(59, 4, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas A', 'm3', '', 0.00, 552118.50),
(60, 4, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas B', 'm3', '', 0.00, 551513.50),
(61, 4, 'DIVISI 5. PERKERASAN BUTIRAN', 'Lapis pondasi agregat kelas C', 'm3', '', 736.63, 259424.12),
(62, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 800', 'm3', '', 1.32, 29445120.00),
(63, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 500', 'm3', '', 8.96, 3010533.00),
(64, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 350', 'm3', '', 369.03, 1971198.88),
(65, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 250', 'm3', '', 707.72, 1746700.88),
(66, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 175', 'm3', '', 282.27, 1074439.38),
(67, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Beton K 125', 'm3', '', 96.29, 755841.62),
(68, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Baja Tulangan U32 Ulir', 'Kg', '', 150479.41, 15637.07),
(69, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pabrikasi dan pemasangan baja bangunan tegangan leleh 2500 kg/cm2', 'Kg', '', 4911.93, 49495.79),
(70, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pengadaan lengkap bangunan atas rangka baja pelengkung', 'Kg', '', 476812.56, 33566.50),
(71, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pemasangan jembatan rangka baja', 'Kg', '', 412779.81, 16958.33),
(72, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Hangar CT Bar dia. 56 mm furbished', 'Kg', '', 1366.11, 184140.00),
(73, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Strand stressing', 'Ton', '', 1.90, 55491184.00),
(74, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pondasi Cerucuk, Penyediaan dan Pemancangan', 'm', '', 1980.00, 8030.40),
(75, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pemancangan Sheet Pile', 'm', '', 560.00, 258880.27),
(76, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Tipe Asphaltic Plig', 'm', '', 11.00, 2062708.38),
(77, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Single Seal', 'Buah', '', 1.00, 334950112.00),
(78, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Expantion Joint Multi Seal', 'Buah', '', 1.00, 625790080.00),
(79, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Pot Bearing', 'Buah', '', 8.00, 320652864.00),
(80, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Sandaran / Railing (Pengadaan)', 'Kg', '', 0.00, 20627.85),
(81, 4, 'DIVISI 7. PEKERJAAN STRUKTUR', 'Sandaran (pemasangan)', 'Kg', '', 19831.20, 13684.37);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `gambaran` text,
  `structure` text,
  `type` varchar(128) DEFAULT NULL,
  `budget` varchar(128) DEFAULT NULL,
  `owner` int(11) NOT NULL,
  `pic` varchar(128) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `title`, `description`, `gambaran`, `structure`, `type`, `budget`, `owner`, `pic`, `create_time`) VALUES
(2, 'Jembatan SIAK 3', '<p style="margin: 0px 0px 10px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Jembatan adalah suatu konstruksi atau struktur yang melintaskan alur jalan melintasi rintangan yang ada tanpa menutupnya. Adapun jenis rintangan tersebut yaitu sungai, jurang, saluran irigasi, jalan raya, jalan kereta api, lembah, laut dan selat. Proyek Pembangunan&nbsp; Jembatan Siak III dilaksanakan untuk meningkatkan dan memperbaiki kondisi jembatan dan juga arus kendaraan darat yang melintasi ruas jalan tersebut. Proyek ini merupakan proyek dari Dinas Pekerjaan Umum Bidang Bina Marga Provinsi Riau. Masa Pelaksanaan adalah 180 hari kalender.</p>\r\n\r\n<p style="margin: 0px 0px 10px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Proyek Pembangunan Jembatan Sei. Siak II tahap ke sepuluh ini terletak pada ruas jalan 006 yaitu jalan penghubung antara kota Pekanbaru &ndash; kota Dumai pada ruas ini kepadatan lalu lintas cukup tinggi, terutama untuk angkutan umum. Dengan dibatasinya penggunaan arus transportasi menggunakan Jembatan Leigthon I maka pertumbuhan lalu lintas sudah tidak mencukupi lagi menggunakan Jembatan Leigthon I, disamping umur rencana jembatan ini semakin berkurang. Tujuan dari Pembangunan Jembatan Sei. Siak III ini adalah untuk mengatasi pertumbuhan arus transportasi dari Kota Pekanbaru &ndash; Kota Dumai atau sebaliknya serta sebagai salah satu penunjang pelaksanaan PON ke XII yang akan diadakan di Pekanbaru pada tahun 2012.</p>\r\n\r\n<p style="margin: 0px 0px 10px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Konstruksi Utama dari Jembatan ini adalah Struktur Rangka Baja Pelengkung.</p>\r\n', '<p><strong>Informasi Proyek</strong></p>', '<p><img alt="" src="/konsol/upload/images/ckk/images/Picture1.png" style="width: 1449px; height: 832px;" /></p>\r\n', 'Struktur Rangka Baja Pelengkung', '35976617115', 1, '/konsol/upload/images/ckk/images/Picture2.jpg', '2013-05-04 02:47:09'),
(3, 'Jembatak Siak 2', '<p style="margin: 0px 0px 10px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Jembatan adalah suatu konstruksi atau struktur yang melintaskan alur jalan melintasi rintangan yang ada tanpa menutupnya. Adapun jenis rintangan tersebut yaitu sungai, jurang, saluran irigasi, jalan raya, jalan kereta api, lembah, laut dan selat. Proyek Pembangunan&nbsp; Jembatan Siak III dilaksanakan untuk meningkatkan dan memperbaiki kondisi jembatan dan juga arus kendaraan darat yang melintasi ruas jalan tersebut. Proyek ini merupakan proyek dari Dinas Pekerjaan Umum Bidang Bina Marga Provinsi Riau. Masa Pelaksanaan adalah 180 hari kalender.</p>\r\n\r\n<p style="margin: 0px 0px 10px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Proyek Pembangunan Jembatan Sei. Siak II tahap ke sepuluh ini terletak pada ruas jalan 006 yaitu jalan penghubung antara kota Pekanbaru &ndash; kota Dumai pada ruas ini kepadatan lalu lintas cukup tinggi, terutama untuk angkutan umum. Dengan dibatasinya penggunaan arus transportasi menggunakan Jembatan Leigthon I maka pertumbuhan lalu lintas sudah tidak mencukupi lagi menggunakan Jembatan Leigthon I, disamping umur rencana jembatan ini semakin berkurang. Tujuan dari Pembangunan Jembatan Sei. Siak III ini adalah untuk mengatasi pertumbuhan arus transportasi dari Kota Pekanbaru &ndash; Kota Dumai atau sebaliknya serta sebagai salah satu penunjang pelaksanaan PON ke XII yang akan diadakan di Pekanbaru pada tahun 2012.</p>\r\n\r\n<p style="margin: 0px 0px 10px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Konstruksi Utama dari Jembatan ini adalah Struktur Rangka Baja Pelengkung.</p>\r\n', '<p>Gambaran</p>\r\n', '<p>Struktur</p>\r\n', 'Konstruksi Baja', '2000000000', 1, '/konsol/upload/images/ckk/images/porto4.jpg', '2013-05-04 03:00:56'),
(4, 'Jembatan SIAK 1', '<p>Penjelasan Umum</p>', '<p><strong>Informasi Proyek</strong></p>', '<p><img alt="" src="/konsol/upload/images/ckk/images/Picture1.png" style="width: 1449px; height: 832px;" /></p>\r\n<p><img alt="" src="/konsol/upload/images/ckk/images/Picture1.png" style="width: 1449px; height: 832px;" /></p>\r\n', 'Struktur Rangka Baja Pelengkung', '35976617115', 1, '/konsol/upload/images/ckk/images/Picture2.jpg', '2013-05-04 02:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `rencana_equipment`
--

CREATE TABLE IF NOT EXISTS `rencana_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_id` int(11) NOT NULL,
  `minggu` int(200) NOT NULL,
  `bulan` int(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a1` (`minggu`),
  KEY `rencana_pekerjaan` (`equipment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `rencana_equipment`
--

INSERT INTO `rencana_equipment` (`id`, `equipment_id`, `minggu`, `bulan`, `tahun`) VALUES
(1, 1, 2, 8, 2013),
(2, 1, 3, 8, 2013),
(3, 1, 4, 8, 2013),
(4, 2, 2, 8, 2013),
(5, 2, 3, 8, 2013),
(6, 2, 4, 8, 2013),
(7, 3, 2, 8, 2013),
(8, 3, 3, 8, 2013),
(9, 3, 4, 8, 2013);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_manpower`
--

CREATE TABLE IF NOT EXISTS `rencana_manpower` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manpower_id` int(11) NOT NULL,
  `minggu` int(200) NOT NULL,
  `bulan` int(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a1` (`minggu`),
  KEY `rencana_pekerjaan` (`manpower_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=224 ;

--
-- Dumping data for table `rencana_manpower`
--

INSERT INTO `rencana_manpower` (`id`, `manpower_id`, `minggu`, `bulan`, `tahun`) VALUES
(89, 14, 4, 7, 2013),
(90, 15, 4, 7, 2013),
(106, 14, 4, 7, 2013),
(107, 15, 4, 7, 2013),
(108, 19, 4, 7, 2013),
(109, 20, 4, 7, 2013),
(110, 21, 1, 8, 2013),
(111, 21, 2, 8, 2013),
(112, 21, 3, 8, 2013),
(113, 21, 4, 8, 2013),
(114, 22, 1, 8, 2013),
(115, 22, 2, 8, 2013),
(116, 22, 3, 8, 2013),
(117, 22, 4, 8, 2013),
(118, 23, 1, 8, 2013),
(119, 23, 2, 8, 2013),
(120, 23, 3, 8, 2013),
(121, 23, 4, 8, 2013),
(122, 21, 4, 7, 2013),
(123, 21, 1, 8, 2013),
(124, 21, 2, 8, 2013),
(125, 21, 3, 8, 2013),
(126, 21, 4, 8, 2013),
(127, 22, 4, 7, 2013),
(128, 22, 1, 8, 2013),
(129, 22, 2, 8, 2013),
(130, 22, 3, 8, 2013),
(131, 22, 4, 8, 2013),
(132, 23, 4, 7, 2013),
(133, 23, 1, 8, 2013),
(134, 23, 2, 8, 2013),
(135, 23, 3, 8, 2013),
(136, 23, 4, 8, 2013),
(137, 24, 4, 7, 2013),
(138, 25, 4, 7, 2013),
(139, 21, 4, 7, 2013),
(140, 21, 1, 8, 2013),
(141, 21, 2, 8, 2013),
(142, 21, 3, 8, 2013),
(143, 21, 4, 8, 2013),
(144, 22, 4, 7, 2013),
(145, 22, 1, 8, 2013),
(146, 22, 2, 8, 2013),
(147, 22, 3, 8, 2013),
(148, 22, 4, 8, 2013),
(149, 23, 4, 7, 2013),
(150, 23, 1, 8, 2013),
(151, 23, 2, 8, 2013),
(152, 23, 3, 8, 2013),
(153, 23, 4, 8, 2013),
(154, 24, 4, 7, 2013),
(155, 25, 4, 7, 2013),
(156, 29, 4, 7, 2013),
(157, 30, 4, 7, 2013),
(190, 1, 4, 7, 2013),
(191, 1, 1, 8, 2013),
(192, 1, 2, 8, 2013),
(193, 1, 3, 8, 2013),
(194, 1, 4, 8, 2013),
(195, 2, 4, 7, 2013),
(196, 2, 1, 8, 2013),
(197, 2, 2, 8, 2013),
(198, 2, 3, 8, 2013),
(199, 2, 4, 8, 2013),
(200, 3, 4, 7, 2013),
(201, 3, 1, 8, 2013),
(202, 3, 2, 8, 2013),
(203, 3, 3, 8, 2013),
(204, 3, 4, 8, 2013),
(205, 4, 4, 7, 2013),
(206, 5, 4, 7, 2013),
(207, 9, 4, 7, 2013),
(208, 10, 4, 7, 2013),
(209, 11, 4, 7, 2013),
(210, 11, 1, 8, 2013),
(211, 11, 2, 8, 2013),
(212, 11, 3, 8, 2013),
(213, 11, 4, 8, 2013),
(214, 12, 4, 7, 2013),
(215, 12, 1, 8, 2013),
(216, 12, 2, 8, 2013),
(217, 12, 3, 8, 2013),
(218, 12, 4, 8, 2013),
(219, 13, 4, 7, 2013),
(220, 13, 1, 8, 2013),
(221, 13, 2, 8, 2013),
(222, 13, 3, 8, 2013),
(223, 13, 4, 8, 2013);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_material`
--

CREATE TABLE IF NOT EXISTS `rencana_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL,
  `minggu` int(200) NOT NULL,
  `bulan` int(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a1` (`minggu`),
  KEY `rencana_pekerjaan` (`material_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `rencana_material`
--

INSERT INTO `rencana_material` (`id`, `material_id`, `minggu`, `bulan`, `tahun`) VALUES
(1, 1, 1, 8, 2013),
(2, 1, 2, 8, 2013),
(3, 1, 3, 8, 2013),
(4, 2, 1, 8, 2013),
(5, 2, 2, 8, 2013),
(6, 2, 3, 8, 2013),
(7, 3, 1, 8, 2013),
(8, 3, 2, 8, 2013),
(9, 3, 3, 8, 2013);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_progress`
--

CREATE TABLE IF NOT EXISTS `rencana_progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `progress_id` int(11) NOT NULL,
  `minggu` int(200) NOT NULL,
  `bulan` int(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a1` (`minggu`),
  KEY `rencana` (`progress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=820 ;

--
-- Dumping data for table `rencana_progress`
--

INSERT INTO `rencana_progress` (`id`, `progress_id`, `minggu`, `bulan`, `tahun`) VALUES
(1, 1, 1, 8, 2013),
(2, 1, 2, 8, 2013),
(3, 1, 3, 8, 2013),
(4, 1, 1, 12, 2013),
(5, 1, 2, 12, 2013),
(6, 2, 1, 8, 2013),
(7, 2, 2, 8, 2013),
(8, 2, 3, 8, 2013),
(9, 2, 3, 11, 2013),
(10, 2, 4, 11, 2013),
(11, 2, 1, 12, 2013),
(12, 3, 3, 11, 2013),
(13, 4, 4, 8, 2013),
(14, 4, 1, 9, 2013),
(15, 4, 2, 9, 2013),
(16, 4, 3, 9, 2013),
(17, 4, 4, 9, 2013),
(18, 4, 1, 10, 2013),
(19, 4, 2, 10, 2013),
(20, 4, 3, 10, 2013),
(21, 4, 4, 10, 2013),
(22, 4, 3, 11, 2013),
(23, 4, 4, 11, 2013),
(24, 6, 2, 11, 2013),
(25, 7, 1, 11, 2013),
(26, 7, 1, 12, 2013),
(27, 8, 3, 11, 2013),
(28, 9, 3, 11, 2013),
(29, 10, 1, 8, 2013),
(30, 10, 2, 8, 2013),
(31, 10, 3, 8, 2013),
(32, 10, 4, 8, 2013),
(33, 10, 1, 9, 2013),
(34, 11, 1, 10, 2013),
(35, 11, 2, 10, 2013),
(36, 11, 3, 10, 2013),
(37, 11, 4, 10, 2013),
(38, 11, 1, 11, 2013),
(39, 11, 2, 11, 2013),
(40, 11, 3, 11, 2013),
(41, 11, 4, 11, 2013),
(42, 12, 2, 9, 2013),
(43, 12, 3, 9, 2013),
(44, 12, 4, 9, 2013),
(45, 12, 1, 10, 2013),
(46, 12, 2, 10, 2013),
(47, 12, 3, 10, 2013),
(48, 12, 4, 10, 2013),
(49, 12, 3, 11, 2013),
(50, 12, 4, 11, 2013),
(51, 12, 1, 12, 2013),
(52, 13, 1, 9, 2013),
(53, 13, 2, 9, 2013),
(54, 13, 3, 9, 2013),
(55, 13, 1, 10, 2013),
(56, 13, 2, 10, 2013),
(57, 13, 3, 11, 2013),
(58, 13, 4, 11, 2013),
(59, 14, 4, 8, 2013),
(60, 14, 1, 9, 2013),
(61, 14, 2, 9, 2013),
(62, 14, 3, 9, 2013),
(63, 14, 4, 9, 2013),
(64, 14, 1, 10, 2013),
(65, 14, 2, 10, 2013),
(66, 14, 3, 10, 2013),
(67, 14, 4, 10, 2013),
(68, 14, 1, 11, 2013),
(69, 14, 3, 11, 2013),
(70, 14, 4, 11, 2013),
(71, 14, 1, 12, 2013),
(72, 15, 4, 8, 2013),
(73, 15, 1, 9, 2013),
(74, 15, 3, 11, 2013),
(75, 15, 4, 11, 2013),
(76, 16, 4, 10, 2013),
(77, 16, 1, 11, 2013),
(78, 16, 2, 11, 2013),
(79, 16, 3, 11, 2013),
(80, 16, 4, 11, 2013),
(81, 16, 1, 12, 2013),
(82, 16, 2, 12, 2013),
(83, 17, 2, 11, 2013),
(84, 17, 3, 11, 2013),
(85, 17, 4, 11, 2013),
(86, 17, 1, 12, 2013),
(87, 18, 4, 11, 2013),
(88, 18, 1, 12, 2013),
(89, 19, 2, 11, 2013),
(90, 19, 4, 11, 2013),
(91, 19, 1, 12, 2013),
(92, 20, 3, 11, 2013),
(93, 21, 1, 10, 2013),
(94, 21, 2, 10, 2013),
(95, 21, 3, 10, 2013),
(96, 21, 4, 10, 2013),
(97, 21, 3, 11, 2013),
(98, 21, 4, 11, 2013),
(99, 21, 1, 12, 2013),
(100, 25, 3, 10, 2013),
(101, 25, 4, 10, 2013),
(102, 25, 1, 11, 2013),
(103, 27, 1, 8, 2013),
(104, 27, 2, 8, 2013),
(105, 27, 3, 8, 2013),
(106, 27, 4, 8, 2013),
(107, 27, 1, 9, 2013),
(108, 27, 2, 9, 2013),
(109, 27, 3, 9, 2013),
(110, 27, 4, 9, 2013),
(111, 27, 1, 10, 2013),
(112, 27, 2, 10, 2013),
(113, 27, 3, 10, 2013),
(114, 27, 4, 10, 2013),
(115, 27, 1, 11, 2013),
(116, 27, 2, 11, 2013),
(117, 28, 1, 8, 2013),
(118, 28, 2, 8, 2013),
(119, 28, 3, 8, 2013),
(120, 28, 1, 12, 2013),
(121, 28, 2, 12, 2013),
(122, 29, 1, 8, 2013),
(123, 29, 2, 8, 2013),
(124, 29, 3, 8, 2013),
(125, 29, 3, 11, 2013),
(126, 29, 4, 11, 2013),
(127, 29, 1, 12, 2013),
(128, 30, 3, 11, 2013),
(129, 31, 4, 8, 2013),
(130, 31, 1, 9, 2013),
(131, 31, 2, 9, 2013),
(132, 31, 3, 9, 2013),
(133, 31, 4, 9, 2013),
(134, 31, 1, 10, 2013),
(135, 31, 2, 10, 2013),
(136, 31, 3, 10, 2013),
(137, 31, 4, 10, 2013),
(138, 31, 3, 11, 2013),
(139, 31, 4, 11, 2013),
(140, 33, 2, 11, 2013),
(141, 34, 1, 11, 2013),
(142, 34, 1, 12, 2013),
(143, 35, 3, 11, 2013),
(144, 36, 3, 11, 2013),
(145, 37, 1, 8, 2013),
(146, 37, 2, 8, 2013),
(147, 37, 3, 8, 2013),
(148, 37, 4, 8, 2013),
(149, 37, 1, 9, 2013),
(150, 38, 1, 10, 2013),
(151, 38, 2, 10, 2013),
(152, 38, 3, 10, 2013),
(153, 38, 4, 10, 2013),
(154, 38, 1, 11, 2013),
(155, 38, 2, 11, 2013),
(156, 38, 3, 11, 2013),
(157, 38, 4, 11, 2013),
(158, 39, 2, 9, 2013),
(159, 39, 3, 9, 2013),
(160, 39, 4, 9, 2013),
(161, 39, 1, 10, 2013),
(162, 39, 2, 10, 2013),
(163, 39, 3, 10, 2013),
(164, 39, 4, 10, 2013),
(165, 39, 3, 11, 2013),
(166, 39, 4, 11, 2013),
(167, 39, 1, 12, 2013),
(168, 40, 1, 9, 2013),
(169, 40, 2, 9, 2013),
(170, 40, 3, 9, 2013),
(171, 40, 1, 10, 2013),
(172, 40, 2, 10, 2013),
(173, 40, 3, 11, 2013),
(174, 40, 4, 11, 2013),
(175, 41, 4, 8, 2013),
(176, 41, 1, 9, 2013),
(177, 41, 2, 9, 2013),
(178, 41, 3, 9, 2013),
(179, 41, 4, 9, 2013),
(180, 41, 1, 10, 2013),
(181, 41, 2, 10, 2013),
(182, 41, 3, 10, 2013),
(183, 41, 4, 10, 2013),
(184, 41, 1, 11, 2013),
(185, 41, 3, 11, 2013),
(186, 41, 4, 11, 2013),
(187, 41, 1, 12, 2013),
(188, 42, 4, 8, 2013),
(189, 42, 1, 9, 2013),
(190, 42, 3, 11, 2013),
(191, 42, 4, 11, 2013),
(192, 43, 4, 10, 2013),
(193, 43, 1, 11, 2013),
(194, 43, 2, 11, 2013),
(195, 43, 3, 11, 2013),
(196, 43, 4, 11, 2013),
(197, 43, 1, 12, 2013),
(198, 43, 2, 12, 2013),
(199, 44, 2, 11, 2013),
(200, 44, 3, 11, 2013),
(201, 44, 4, 11, 2013),
(202, 44, 1, 12, 2013),
(203, 45, 4, 11, 2013),
(204, 45, 1, 12, 2013),
(205, 46, 2, 11, 2013),
(206, 46, 4, 11, 2013),
(207, 46, 1, 12, 2013),
(208, 47, 3, 11, 2013),
(209, 48, 1, 10, 2013),
(210, 48, 2, 10, 2013),
(211, 48, 3, 10, 2013),
(212, 48, 4, 10, 2013),
(213, 48, 3, 11, 2013),
(214, 48, 4, 11, 2013),
(215, 48, 1, 12, 2013),
(216, 52, 3, 10, 2013),
(217, 52, 4, 10, 2013),
(218, 52, 1, 11, 2013),
(219, 54, 1, 8, 2013),
(220, 54, 2, 8, 2013),
(221, 54, 3, 8, 2013),
(222, 54, 4, 8, 2013),
(223, 54, 1, 9, 2013),
(224, 54, 2, 9, 2013),
(225, 54, 3, 9, 2013),
(226, 54, 4, 9, 2013),
(227, 54, 1, 10, 2013),
(228, 54, 2, 10, 2013),
(229, 54, 3, 10, 2013),
(230, 54, 4, 10, 2013),
(231, 54, 1, 11, 2013),
(232, 54, 2, 11, 2013),
(233, 55, 1, 8, 2013),
(234, 55, 2, 8, 2013),
(235, 55, 3, 8, 2013),
(236, 55, 1, 12, 2013),
(237, 55, 2, 12, 2013),
(238, 56, 1, 8, 2013),
(239, 56, 2, 8, 2013),
(240, 56, 3, 8, 2013),
(241, 56, 3, 11, 2013),
(242, 56, 4, 11, 2013),
(243, 56, 1, 12, 2013),
(244, 57, 3, 11, 2013),
(245, 58, 4, 8, 2013),
(246, 58, 1, 9, 2013),
(247, 58, 2, 9, 2013),
(248, 58, 3, 9, 2013),
(249, 58, 4, 9, 2013),
(250, 58, 1, 10, 2013),
(251, 58, 2, 10, 2013),
(252, 58, 3, 10, 2013),
(253, 58, 4, 10, 2013),
(254, 58, 3, 11, 2013),
(255, 58, 4, 11, 2013),
(256, 60, 2, 11, 2013),
(257, 61, 1, 11, 2013),
(258, 61, 1, 12, 2013),
(259, 62, 3, 11, 2013),
(260, 63, 3, 11, 2013),
(261, 64, 1, 8, 2013),
(262, 64, 2, 8, 2013),
(263, 64, 3, 8, 2013),
(264, 64, 4, 8, 2013),
(265, 64, 1, 9, 2013),
(266, 65, 1, 10, 2013),
(267, 65, 2, 10, 2013),
(268, 65, 3, 10, 2013),
(269, 65, 4, 10, 2013),
(270, 65, 1, 11, 2013),
(271, 65, 2, 11, 2013),
(272, 65, 3, 11, 2013),
(273, 65, 4, 11, 2013),
(274, 66, 2, 9, 2013),
(275, 66, 3, 9, 2013),
(276, 66, 4, 9, 2013),
(277, 66, 1, 10, 2013),
(278, 66, 2, 10, 2013),
(279, 66, 3, 10, 2013),
(280, 66, 4, 10, 2013),
(281, 66, 3, 11, 2013),
(282, 66, 4, 11, 2013),
(283, 66, 1, 12, 2013),
(284, 67, 1, 9, 2013),
(285, 67, 2, 9, 2013),
(286, 67, 3, 9, 2013),
(287, 67, 1, 10, 2013),
(288, 67, 2, 10, 2013),
(289, 67, 3, 11, 2013),
(290, 67, 4, 11, 2013),
(291, 68, 4, 8, 2013),
(292, 68, 1, 9, 2013),
(293, 68, 2, 9, 2013),
(294, 68, 3, 9, 2013),
(295, 68, 4, 9, 2013),
(296, 68, 1, 10, 2013),
(297, 68, 2, 10, 2013),
(298, 68, 3, 10, 2013),
(299, 68, 4, 10, 2013),
(300, 68, 1, 11, 2013),
(301, 68, 3, 11, 2013),
(302, 68, 4, 11, 2013),
(303, 68, 1, 12, 2013),
(304, 69, 4, 8, 2013),
(305, 69, 1, 9, 2013),
(306, 69, 3, 11, 2013),
(307, 69, 4, 11, 2013),
(308, 70, 4, 10, 2013),
(309, 70, 1, 11, 2013),
(310, 70, 2, 11, 2013),
(311, 70, 3, 11, 2013),
(312, 70, 4, 11, 2013),
(313, 70, 1, 12, 2013),
(314, 70, 2, 12, 2013),
(315, 71, 2, 11, 2013),
(316, 71, 3, 11, 2013),
(317, 71, 4, 11, 2013),
(318, 71, 1, 12, 2013),
(319, 72, 4, 11, 2013),
(320, 72, 1, 12, 2013),
(321, 73, 2, 11, 2013),
(322, 73, 4, 11, 2013),
(323, 73, 1, 12, 2013),
(324, 74, 3, 11, 2013),
(325, 75, 1, 10, 2013),
(326, 75, 2, 10, 2013),
(327, 75, 3, 10, 2013),
(328, 75, 4, 10, 2013),
(329, 75, 3, 11, 2013),
(330, 75, 4, 11, 2013),
(331, 75, 1, 12, 2013),
(332, 79, 3, 10, 2013),
(333, 79, 4, 10, 2013),
(334, 79, 1, 11, 2013),
(335, 81, 1, 8, 2013),
(336, 81, 2, 8, 2013),
(337, 81, 3, 8, 2013),
(338, 81, 4, 8, 2013),
(339, 81, 1, 9, 2013),
(340, 81, 2, 9, 2013),
(341, 81, 3, 9, 2013),
(342, 81, 4, 9, 2013),
(343, 81, 1, 10, 2013),
(344, 81, 2, 10, 2013),
(345, 81, 3, 10, 2013),
(346, 81, 4, 10, 2013),
(347, 81, 1, 11, 2013),
(348, 81, 2, 11, 2013);

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

CREATE TABLE IF NOT EXISTS `thread` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(10) unsigned NOT NULL,
  `subject` varchar(120) NOT NULL,
  `is_sticky` tinyint(1) unsigned NOT NULL,
  `is_locked` tinyint(1) unsigned NOT NULL,
  `view_count` bigint(20) unsigned NOT NULL,
  `created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_thread_forum` (`forum_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `thread`
--

INSERT INTO `thread` (`id`, `forum_id`, `subject`, `is_sticky`, `is_locked`, `view_count`, `created`) VALUES
(1, 2, 'First release', 1, 0, 30, 0),
(2, 5, 'Subject is allowed to be blank when creating a new thread', 0, 0, 4, 0),
(3, 5, 'Post date is not set', 0, 1, 17, 0),
(4, 5, 'Forum view does not show correct last post', 0, 1, 11, 1349540563),
(5, 6, 'User signatures', 0, 0, 1, 1349570366),
(6, 6, 'BB Code', 0, 0, 1, 1349570413),
(7, 5, 'Attachments', 0, 0, 21, 1349578699),
(8, 2, 'yes', 0, 0, 1, 1368034409),
(9, 2, 'yes', 0, 0, 1, 1368034434);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(128) NOT NULL,
  `position` varchar(128) NOT NULL,
  `salt` varchar(128) DEFAULT NULL,
  `access_previleges` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`, `position`, `salt`, `access_previleges`) VALUES
(1, 'test', '89c1900e4dddeea9915eb8e554745293', 'admin', '', '510cc23b02fc82.79083121', NULL),
(2, 'manpower', '38e0785bfc42f920959313834d39df39', 'manpower', '', '51a1be6617ca73.16069505', NULL),
(3, 'equipment', '0835c6822d566dcab37327a709ebc187', 'equipment', '', '51a1c4eb960905.04820105', NULL),
(4, 'material', '770f47f757d5e2248695a0e1f2995413', 'material', '', '51a1c4f7280996.67651000', NULL),
(5, 'engineering', 'df02dcff22dd90bb354d91458ee50777', 'engineering', '', '51a1c5089fad24.83798414', NULL),
(6, 'keuangan', '44fb948ef570e8995537d41f20259043', 'keuangan', '', '51a5b7cda90082.14822715', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `type` varchar(128) DEFAULT NULL,
  `detail` text,
  `start_time` datetime NOT NULL,
  `finish_time` datetime DEFAULT NULL,
  `video` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `r6` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `project_id`, `name`, `type`, `detail`, `start_time`, `finish_time`, `video`) VALUES
(1, 2, 'hello', 'hello', '<p>hello</p>\r\n', '2013-05-10 17:59:33', '2013-05-10 17:59:33', 'http://www.youtube.com/watch?v=9c6W4CCU9M4');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktual_progress`
--
ALTER TABLE `aktual_progress`
  ADD CONSTRAINT `aktual_progress` FOREIGN KEY (`progress_id`) REFERENCES `progress` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `drawing`
--
ALTER TABLE `drawing`
  ADD CONSTRAINT `drawing_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `equipment`
--
ALTER TABLE `equipment`
  ADD CONSTRAINT `equipment_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `external`
--
ALTER TABLE `external`
  ADD CONSTRAINT `id_procurement` FOREIGN KEY (`procurement_id`) REFERENCES `procurement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum`
--
ALTER TABLE `forum`
  ADD CONSTRAINT `FK_forum_forum` FOREIGN KEY (`parent_id`) REFERENCES `forum` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `manpower`
--
ALTER TABLE `manpower`
  ADD CONSTRAINT `manpower_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `page_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_post_author` FOREIGN KEY (`author_id`) REFERENCES `forumuser` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_post_editor` FOREIGN KEY (`editor_id`) REFERENCES `forumuser` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_post_thread` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `progress`
--
ALTER TABLE `progress`
  ADD CONSTRAINT `pekerjaan_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rencana_equipment`
--
ALTER TABLE `rencana_equipment`
  ADD CONSTRAINT `rencana_equipmen` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rencana_manpower`
--
ALTER TABLE `rencana_manpower`
  ADD CONSTRAINT `manpower_project` FOREIGN KEY (`manpower_id`) REFERENCES `manpower` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rencana_material`
--
ALTER TABLE `rencana_material`
  ADD CONSTRAINT `material_projects` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rencana_progress`
--
ALTER TABLE `rencana_progress`
  ADD CONSTRAINT `rencana` FOREIGN KEY (`progress_id`) REFERENCES `progress` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thread`
--
ALTER TABLE `thread`
  ADD CONSTRAINT `FK_thread_forum` FOREIGN KEY (`forum_id`) REFERENCES `forum` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
